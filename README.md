# [MySQLweb]
MySQLweb is a web application that run queries, create and edit data, and view and export results, created and maintained by [Gianfranco Messori].

### Installation
#### Quick Start
* `git clone https://gieffe@bitbucket.org/gieffe/mysqlweb.git` or [download source from Bitbucket][1] 
* `cd mysqlweb/bin`
* `./mysqlweb-server start`
* Point your browser to http://localhost:8082/

#### Docker Quick Start (new)
+ [Docker Readme]

#### Dependencies
MySQLweb runs on Python from 2.4 up to 2.7 and should run on any Unix-like platform. It has dependencies with the following libraries:

+ [MySQLdb]
+ [Twisted]

#### Site Configuration
`mysqlweb-server` first start create a default configuration file in `bin/site_config.py`. Edit this file to change your server installation:

* DNS: domain name server [localhost]
* IP_ADDRESS, HTTP_PORT: the server listen to this ip/port [127.0.0.1, 8082]
* AUTH_IP: list of the ip address authorized to access your installation of mysqlweb [['127.0.0.1',]].

If you want create default configuration file before starting mysqlweb for the first time, run these commands:

* `cd mysqlweb/bin`
* `python config.py`


> **Be careful!** MySQLweb was made to run over a local *intranet* protected by armoured firewall. I advise developer against running the application over the internet.  
>Moreover, if your data are important i suggest to run MySQLweb behind a reverse proxy with https support (like [nginx] for example), or connect to mysql using an SSH tunnel (see below).

### Using MySQLweb
MySQLweb is a web application that run queries, create and edit data, and view and export results. But there is more ...

#### DataBase Structure  
  Database Structure Browser, provides instant access to database schema and objects. 

#### Connection Manager  
  The Database Connections Panel enables developers to create, organize, and manage database connections 

#### History Panel  
  The History Panel provides complete session history of queries showing what queries were run and when. Developers can easily retrieve, review, re-run, append or modify previously executed SQL statement. 

#### Exec Procedures  
  Multiple queries can be executed simultaneously (in transaction - query have to be separated by semicolon) 

#### Save SQL  
  Developers can save and easily reuse  common Queries 

#### Dump SQL
  *Dump SQL* dumps results of a query to a file contains SQL insert statements.
  These stataments can be used for backup or transfer data to another SQL server.

#### Query Autocomplete  
  Press "esc" when you are typing queries and enjoy with Autocoplete-query feature! 

#### Query Wizard  
  Type wsel tablename [,tablename 2, ...] and run query: MySQLweb prepare for you select statement to query the specified tables.
  Type wins tablename and run query: MySQLweb prepares for you insert statement. 

#### Silk Icons Search  
  Type silk icon-pattern and run query: MySQLweb search Silk icon that matches the pattern specified. 

### Connect to a MySQL database using an SSH Tunnel

You can setup an SSH tunnel using command like this from Terminal:

	$ ssh -l youruser yourhost.com -p 22 -N -f -C -L 1234:yourdbserver.com:3306

Now, to connect to mysql database over SSH Tunnel, go to Connection Manager of MySQLweb and set *Host*  to `127.0.0.1:1234` instead of `mysqlhost`.

**Argument summary:**

* -l login name
* -p remote host port (It is best to connect to ssh on something other than the default port to shake off automated attacks. Change sshd.conf and/or the port mapping on your firewall. For example :2210 external maps to :22 internal for your ssh boxes that are allowed to accept outside connections.)
* -N do not execute a remote command
* -f requests SSH to go to background
* -L port:host:hostport (port = local port, host and hostport are where you want the tunnel to point to. This does not have to be the box you are ssh-ing to!)
* -C compression – optional

### Keyboard Shortcuts
- Run current query:  `cmd+enter` or `ctrl+enter`
- Comment selection: `cmd+k` or `ctrl+k`
- Completion: `esc`


### LICENSE
[MIT]

[Gianfranco Messori]: https://bitbucket.org/gieffe
[MySQLweb]: https://bitbucket.org/gieffe/mysqlweb
[MySQLdb]: http://mysql-python.sourceforge.net/
[Twisted]: http://twistedmatrix.com/trac/wiki
[1]: https://bitbucket.org/gieffe/mysqlweb
[MIT]: http://opensource.org/licenses/mit-license.php
[nginx]: http://nginx.org/
[Docker Readme]: docker/README.md
