### Run MySQLweb with Docker

* Create a directory on the docker host to store MySQLweb persistent data
  (configuration file, users preferences, connection and query history)

* Create a directory named `config` inside this directory

* Inside directory `mysqlweb-data`, create `site_config.yaml` based upon `site_config.yaml.orig`:

		APPSERVER_URI: ''
		AUTH_IP:
		  - '127.0.0.1'
		  - '172.19.0.1'
		  - '172.20.0.1'
		  - '172.21.0.1'
		BASE_DIR: '/app/mysqlweb'
		DNS: 'localhost'
		HTTP_PORT: 8088
		IP_ADDRESS: '0.0.0.0'
		MAX_ROW: 100
		STORE_PASSWORD: True
		APPLICATION_LOG_FILE: 'stdout'
		DATA_DIR: '/app/mysqlweb/data'


* The only things to change are the *AUTH_IP* list.

* Edit `docker-compose.yaml` file:


		version: '3'
		services:
		  mysqlweb:
			build: 
			  context: ..
			  dockerfile: docker/Dockerfile
			image: mysqlweb:alpine
			ports:
			- "8082:8088"
			volumes:
			- ./mysqlweb-data:/app/mysqlweb/data
			environment:
			 - SITE_CONFIG_YAML=/app/mysqlweb/data/config/site_config.yaml

			command: ["python", "-u", "mysqlweb/bin/mysqlweb-server", "run"]


* Change `8082` with the exposed port accessible on the host for MySQLweb
  service.

* **Finally**:

		cd docker
		docker-compose up --build

	
