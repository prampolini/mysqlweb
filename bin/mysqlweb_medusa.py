import sys
import traceback
import string
import re
import StringIO
import time
import types
import shelve
import urllib
import os

import config
import auth
import tools
import home


def main(request):
	try:
		start = time.time()
		sys.stderr = sys.stdout
		pard = {}
		stop = 0
		ip = ''
		user = ''
		html = ''
		error_log = ''
		error_sid = ''
		pard['header'] = 'Content-type: text/html\n\n'
		pard['ERROR'] = ''
		pard['html'] = ''
		reload(config)
		pard = config.global_parameters(pard)
		buf = sys.stdin.read()
		#dump(request)
		#tools.dump(pard)
		if buf:
			payload = parse_buf(buf) # trattamento multipart form (gestione file upload)
			if payload: # multipart form
				for field in payload:
					if field.has_key('filename'):
						if field['filename']:
							pard = tratta_file_upload(pard, field)
					else:
						key = urllib.unquote_plus(field['key'])
						value = urllib.unquote_plus(field['value'])
						if pard.has_key(key):
							if type(pard[key]) == types.StringType:
								pard[key] = [pard[key]]
							pard[key].append(value)
						else:
							pard[key] = value
				payload = '' # cosi non entra nel giro normale
			else:
				payload = buf
		else:
			if string.find(request.uri, '?') > -1:
				payload = string.split(request.uri, '?')[1]
			else:
				payload = ''
		if payload:
			payload = urllib.unquote_plus(payload)
			payload = string.split(payload, '&')
			for field in payload:
				(key, value) = string.split(field, '=')
				if pard.has_key(key):
					if type(pard[key]) == types.StringType:
						pard[key] = [pard[key]]
					pard[key].append(value)
				else:
					pard[key] = value
		pard['COMMAND'] = request.command
		pard['HEADER'] = request.header
		for item in request.header:
			header = string.split(item, ': ') ### note space after colon!!!
			pard[header[0]] = string.strip(header[1])
		pard['REPLY_HEADERS'] = request.reply_headers
		pard['REQUEST'] = request.request
		pard['URI'] = request.uri
		pard['HTTP_REFERER'] = pard.get('Referer', '')
		pard['REMOTE_ADDR'] = pard.get('X-Forwarded-For', request.channel.addr)
		if type(pard['REMOTE_ADDR']) == types.TupleType:
			pard['REMOTE_ADDR'] = str(pard['REMOTE_ADDR'][0])
		if len(string.split(pard['REMOTE_ADDR'], ',')) > 1:
			pard['REMOTE_ADDR'] = string.split(pard['REMOTE_ADDR'], ',')[0]
		if string.find(pard['REMOTE_ADDR'], 'unknown') > -1:
			pard['REMOTE_ADDR'] = '127.0.0.1'

		pard = auth.check_sid(pard)
		if pard['flag_sid'] == 'NEW':
			pard['tmp_sid'] = auth.calc_sid(pard)
			pard['module'] = 'auth'
			pard['program'] = 'login'
		
		pard['program'] = pard.get('program', 'main')
		command = pard['module'] + '.' + pard['program'] + '(pard)'
		if command in ['auth.login(pard)', 'auth.check_authentication(pard)']:
			flag = 'LOGIN'
		else:
			pard = auth.load_sid_data(pard)
			if pard['module'] in pard['AUTH_MODULE']:
				flag = 'GO'
			else:
				flag = 'STOP'
		
		if flag in ('GO', 'LOGIN'):
			if pard['DEBUG'] == 1:
				import query
				reload(query)
			if __import__(pard['module']).__dict__.has_key(pard['program']):
				pard = __import__(pard['module']).__dict__[pard['program']](pard)
		else:
			pard = home.build_not_authorized(pard)
		
		
# 		exec('import ' + pard['module'])
# 		if pard['DEBUG'] == 1:
# 			reload(eval(pard['module']))
# 		command = pard['module'] + '.' + pard['program'] + '(pard)'
# 		if command not in ['auth.login(pard)', 'auth.check_authentication(pard)']:
# 			pard = auth.load_sid_data(pard)
# 			if (pard['module'] + '.' + pard['program']) in pard['sid_autorizzazioni']:
# 				pard = eval(command)
# 			else:
# 				pard = home.build_not_authorized(pard)
# 		else:
# 			pard = eval(command)
		stop = time.time()
		pard['sid_user'] = pard.get('sid_user', '')
		error_log = log_hit(start, stop, pard['REMOTE_ADDR'], pard['sid_user'], command, len(pard['html']), pard['ERROR'], pard)
		error_sid = log_sid(start, stop, len(html), pard)
		header = string.strip(string.split(pard['header'], ':')[1])
		request.reply_headers['Content-Type'] = header
		if request.reply_headers['Content-Type'] == 'text/html':
			pard['html'] = string.replace(pard['html'], '\t', '')
			exec_time = repr(stop - start)[0:5]
			#pard['html'] += '\n<br><br><center><font size=1>Exec Time: ' + exec_time + '</font></center>'
			#print '<br><br><center><font size=1>Exec Time: ' + exec_time + '</font></center>'
		print pard['html']
	except:
		stop = time.time()
		pard['ERROR'] = build_exception()
		print pard['ERROR']
		pard['sid_user'] = pard.get('sid_user', '')
		error_log = log_hit(start, stop, pard['REMOTE_ADDR'], pard['sid_user'], command, len(pard['html']), pard['ERROR'], pard)
		error_sid = log_sid(start, stop, len(pard['html']), pard)
	

def log_hit(start, stop, ip, id_utente, command, bytes, error, pard):
	try:
		exec_time = repr(stop - start)[0:5]
		log_file = open(pard['APPLICATION_LOG_FILE'], 'a')
		log_date = time.asctime(time.localtime(time.time()))
		error_id = ''
		if error != '':
			error_id = string.split(repr(stop), '.')[0]
			error = string.replace(error[0], '\n', '\040')
			error = 'ERROR_FILE -> ' + error_id + '.err'
		log_buf = string.join([log_date, id_utente, repr(bytes), exec_time, command, pard['HTTP_REFERER'], error], '\t') + '\n'
		log_file.write(log_buf)
		log_file.close()
		if error_id != '':
			error_file = pard['LOG_DIR'] + '/' + error_id + '.err'
			fp = open(error_file, 'w')
			fp.write(error + '\n')
			keys = pard.keys()
			keys.sort()
			for k in keys:
				buf = repr(k) + ': ' + repr(pard[k]) + '\n'
				fp.write(buf)
			fp.close()
		return ''
	except:
		return build_exception()


def log_sid(start, stop, bytes, pard):
	if pard.has_key('sid'):
		try:
			exec_time = repr(stop - start)[0:5]
			sid_file = auth.sid_get(pard)
			log_date = time.asctime(time.localtime(time.time()))
			pard['EXEC_TIME'] = exec_time
			pard['LOG_DATE'] = log_date
			pard['BYTES'] = repr(bytes)
			sid_file['sid_buffer'] = pard.get('sid_buffer', '')
			result = auth.sid_set(pard, sid_file)
			return ''
		except:
			return build_exception()


def build_exception(type=None, value=None, tb=None, limit=None):
	if type is None:
		type, value, tb = sys.exc_info()
	buf = "<H3>Traceback (most recent call last):</H3>"
	list = traceback.format_tb(tb, limit) + traceback.format_exception_only(type, value)
	buf = buf + "<PRE>%s<B>%s</B></PRE>" % (escape("".join(list[:-1])), escape(list[-1]))
	del tb
	return buf


def escape(s, quote=None):
	"""Replace special characters '&', '<' and '>' by SGML entities."""
	s = s.replace("&", "&amp;") # Must be done first!
	s = s.replace("<", "&lt;")
	s = s.replace(">", "&gt;")
	if quote:
		s = s.replace('"', "&quot;")
	return s


def dump(request):
# 	print '<b>DEFAULT_ERROR_MESSAGE</b><br>'
# 	print repr(request.DEFAULT_ERROR_MESSAGE) + '<br>'
# 	print '<b>__doc__</b><br>'
# 	print repr(request.__doc__) + '<br>'
# 	print '<b>__getitem__</b><br>'
#  	print repr(request.__getitem__) + '<br>'
#  	print '<b>__init__</b><br>'
# 	print repr(request.__init__) + '<br>'
# 	print '<b>__module__</b><br>'
# 	print repr(request.__module__) + '<br>'
# 	print '<b>__setitem__</b><br>'
# 	print repr(request.__setitem__) + '<br>'
# 	print '<b>_header_cache</b><br>'
# 	print '<b>' + repr(request.command) + '<br>'
# 	print repr(dir(request._header_cache)) + '<br>'
# 	print '<b>_split_uri</b><br>'
# 	print '<b>' + repr(request._split_uri) + '<br>'
# 	print repr(dir(request._split_uri)) + '<br>'
	print '<b>build_reply_header:</b><br>'
	print '<b>' + repr(request.build_reply_header) + '</b><br>'
	print repr(dir(request.build_reply_header)) + '<br><br>'
	print '<b>channel:</b><br>'
	print '<b>' + repr(request.channel) + '</b><br>'
	print repr(dir(request.channel)) + '<br><br>'
	print '<b>collect_incoming_data:</b><br>'
	print '<b>' + repr(request.collect_incoming_data) + '</b><br>'
	print repr(dir(request.collect_incoming_data)) + '<br><br>'
	print '<b>collector:</b><br>'
	print '<b>' + repr(request.collector) + '</b><br>'
	print repr(dir(request.collector)) + '<br><br>'
	print '<b>command:</b><br>'
	print '<b>' + repr(request.command) + '</b><br>'
	print repr(dir(request.command)) + '<br><br>'
	print '<b>done:</b><br>'
	print '<b>' + repr(request.done) + '</b><br>'
	print repr(dir(request.done)) + '<br><br>'
	print '<b>error:</b><br>'
	print '<b>' + repr(request.error) + '</b><br>'
	print repr(dir(request.error)) + '<br><br>'
	print '<b>found_terminator:</b><br>'
	print '<b>' + repr(request.found_terminator) + '</b><br>'
	print repr(dir(request.found_terminator)) + '<br><br>'
	print '<b>get_header:</b><br>'
	print '<b>' + repr(request.get_header) + '</b><br>'
	print repr(dir(request.get_header)) + '<br><br>'
	print '<b>get_header_with_regex:</b><br>'
	print '<b>' + repr(request.get_header_with_regex) + '</b><br>'
	print repr(dir(request.get_header_with_regex)) + '<br><br>'
	print '<b>has_key:</b><br>'
	print '<b>' + repr(request.has_key) + '</b><br>'
	print repr(dir(request.has_key)) + '<br><br>'
	print '<b>header:</b><br>'
	print '<b>' + repr(request.header) + '</b><br>'
	print repr(dir(request.header)) + '<br><br>'
	print '<b>log:</b><br>'
	print '<b>' + repr(request.log) + '</b><br>'
	print repr(dir(request.log)) + '<br><br>'
	print '<b>log_date_string:</b><br>'
	print '<b>' + repr(request.log_date_string) + '</b><br>'
	print repr(dir(request.log_date_string)) + '<br><br>'
	print '<b>module:</b><br>'
	print '<b>' + repr(request.module) + '</b><br>'
	print repr(dir(request.module)) + '<br><br>'
	print '<b>outgoing:</b><br>'
	print '<b>' + repr(request.outgoing) + '</b><br>'
	print repr(dir(request.outgoing)) + '<br><br>'
	print '<b>path_regex:</b><br>'
	print '<b>' + repr(request.path_regex) + '</b><br>'
	print repr(dir(request.path_regex)) + '<br><br>'
	print '<b>push:</b><br>'
	print '<b>' + repr(request.push) + '</b><br>'
	print repr(dir(request.push)) + '<br><br>'
	print '<b>reply_code:</b><br>'
	print '<b>' + repr(request.reply_code) + '</b><br>'
	print repr(dir(request.reply_code)) + '<br><br>'
	print '<b>reply_headers:</b><br>'
	print '<b>' + repr(request.reply_headers) + '</b><br>'
	print repr(dir(request.reply_headers)) + '<br><br>'
	print '<b>reply_now:</b><br>'
	print '<b>' + repr(request.reply_now) + '</b><br>'
	print repr(dir(request.reply_now)) + '<br><br>'
	print '<b>request:</b><br>'
	print '<b>' + repr(request.request) + '</b><br>'
	print repr(dir(request.request)) + '<br><br>'
	print '<b>request_counter:</b><br>'
	print '<b>' + repr(request.request_counter) + '</b><br>'
	print repr(dir(request.request_counter)) + '<br><br>'
	print '<b>request_number:</b><br>'
	print '<b>' + repr(request.request_number) + '</b><br>'
	print repr(dir(request.request_number)) + '<br><br>'
	print '<b>response:</b><br>'
	print '<b>' + repr(request.response) + '</b><br>'
	print repr(dir(request.response)) + '<br><br>'
	print '<b>responses:</b><br>'
	print '<b>' + repr(request.responses) + '</b><br>'
	print repr(dir(request.responses)) + '<br><br>'
	print '<b>split_uri:</b><br>'
	print '<b>' + repr(request.split_uri) + '</b><br>'
	print repr(dir(request.split_uri)) + '<br><br>'
	print '<b>uri:</b><br>'
	print '<b>' + repr(request.uri) + '</b><br>'
	print repr(dir(request.uri)) + '<br><br>'
	print '<b>use_chunked:</b><br>'
	print '<b>' + repr(request.use_chunked) + '</b><br>'
	print repr(dir(request.use_chunked)) + '<br><br>'
	print '<b>version:</b><br>'
	print '<b>' + repr(request.version) + '</b><br>'
	print repr(dir(request.version)) + '<br>'


def parse_buf(buf):
	"""
	input --> stream http rfc2822
	output --> una lista di dizionari con chiavi:
	           key --> nome del campo della form
	           value --> valore del campo o contenuto del file
	           filename --> presente solo se il campo e' un file_upload
	                        contiene il nome del file caricato.
	"""
	ll = string.split(buf, '\r\n')
	inizio_blocco = '^--+[0-9a-zA-Z]+$'
	fine_buf      = '^--+[0-9a-zA-Z]+--$'
	result = []
	CD_ok = False
	for line in ll:
		if not line:
			continue
		
		# ----------------- inizio_blocco -----------------
		s = re.search(inizio_blocco, line)
		if s:
			if CD_ok:
				item['value'] = string.join(item['value'], '\n')
				result.append(item)
			CD_ok = False
			item = {'value': []}
			continue
		
		# ----------------- fine buffer -----------------
		s = re.search(fine_buf, line)
		if s:
			if CD_ok:
				item['value'] = string.join(item['value'], '\n')
				result.append(item)
				break
		
		# ----------------- Content-Disposition -----------------
		s = re.search('^Content-Disposition: form-data;', line)
		if s:
			CD_ok = True # ci deve essere almeno un Content-Disposition per ogni blocco
			form_data = string.split(line, ';')
			form_data = map(string.strip, form_data)
			for elem in form_data[1:]:
				keytype, keyname = string.split(elem, '=')
				keyname = string.replace(keyname, '"', '')
				if keytype == 'name':
					item['key'] = keyname
				elif keytype == 'filename':
					item['filename'] = keyname
				else:
					item['key'] = 'FORM_ERROR'
			continue
		
		# ----------------- Content-Type: -----------------
		s = re.search('Content-Type:', line)
		if s:
			continue
		
		# ----------------- Value -----------------
		if CD_ok:
			item['value'].append(line)
	
	return result


def tratta_file_upload(pard, field):
	pard['file_upload'] = {}
	pard['file_upload']['filename'] = urllib.unquote_plus(field['filename'])
	pard['file_upload']['content'] = field['value']
	return pard
