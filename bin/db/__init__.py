import mysql 
import sqlite
import oracle

from dbapi import DatabaseError

class Wrapper(object):
	def __init__(self, fname):
		self.fname = fname
		#print '**Wrapper**', fname
	def __call__(self, pard, **args):
		pard.setdefault('DB', '')
		if pard['DB'].startswith('sqlite:'):
			result = getattr(sqlite.Sqlite, self.fname)(pard, **args)
		elif pard['DB'].startswith('oracle:'):
			result = getattr(oracle.Oracle, self.fname)(pard, **args)
		else:
			result = getattr(mysql.MySQL, self.fname)(pard, **args)		
		return result


class DBaccess(object):
	def __init__(self):
		self.methods = {}
	def __getattr__(self, name):
		if name not in self.methods:
			self.methods[name] = Wrapper(name)
		return self.methods[name]

db_access = DBaccess()

def cfg(engine=''):
	import config
	import getpass
	pard = config.global_parameters({})
	if engine == 'sqlite':
		pard['HOST'] = ''
		pard['USER'] = ''
		pard['PASSWORD'] = '******'
		pard['DB'] = 'sqlite:/private/home/gm/altavoce/books.sqlite'
	elif engine == 'mysql':
		pard['HOST'] = 'localhost'
		pard['USER'] = 'devel'
		pard['PASSWORD'] = getpass.getpass()
		pard['DB'] = 'devel'
	elif engine == 'oracle':
		pard['HOST'] = 'test'
		pard['USER'] = 'tmprod'
		pard['PASSWORD'] = getpass.getpass()
		pard['DB'] = 'oracle:'
	else:
		pard['HOST'] = 'localhost'
		pard['USER'] = 'devel'
		pard['DB'] = 'devel'
		host = raw_input('host [localhost]: ')
		user = raw_input('user [devel]: ')
		db   = raw_input('db [devel]:')
		pard['PASSWORD'] = getpass.getpass()
		if host:
			pard['HOST'] = host
		if user:
			pard['USER'] = user
		if db:
			pard['DB'] = db
	return pard

"""
To test db_access method from python interpreter:

>>> import db
>>> pard = db.cfg('mysql')
>>> db.db_access.method(pard)
>>> ...
"""

if __name__ == '__main__':
	pass