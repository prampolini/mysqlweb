import sqlite3

from dbapi import DBApi, DatabaseError

SqliteError = sqlite3.Error

class Sqlite(DBApi):
	
	@classmethod
	def connect(cls, pard):
		db = pard['DB']
		## db = 'sqlite:database_name'
		try:
			conn = sqlite3.connect(db[7:])
			conn.text_factory = str
		except SqliteError, err:
			raise DatabaseError(str(err))
		return conn
	
	@classmethod
	def send(cls, pard):
		try:
			return super(Sqlite, cls).send(pard)
		except SqliteError, err:
			raise DatabaseError(str(err))
		
	@classmethod
	def send_dict(cls, pard):
		try:
			return super(Sqlite, cls).send_dict(pard)
		except SqliteError, err:
			raise DatabaseError(str(err))
	
	@classmethod
	def get_db_tables_list(cls, pard):
		"""Return the tables lists in "pard['DB']" database"""
		pard['sql'] = "SELECT name FROM sqlite_master WHERE type='table'"
		result = cls.send(pard)
		tables_list = [rec[0] for rec in result]
		return tables_list
	
	@classmethod
	def get_db_tables_list_like(cls, pard):
		"""Return the tables lists in "pard['DB']" database
		where table name is like pard['table_search']"""
		pard['sql'] = """
			SELECT name 
			FROM   sqlite_master
			WHERE  type='table'
			and    name like '%(table_search)s%%'
			""" % pard
		result = cls.send(pard)
		tables_list = [rec[0] for rec in result]
		return tables_list
	
	@classmethod
	def get_primary_key(cls, pard):
		"""Return the primary key columns lists for pard['db_table']"""
		pard['fetch_all_row'] = True
		pard['sql'] = "PRAGMA table_info(%(db_table)s)" % pard
		result = cls.send_dict(pard)
		primary_key = []
		for rec in result['rows']:
			if rec['pk'] == 1:
				primary_key.append(rec['name'])
		return primary_key
	
	@classmethod
	def show_columns(cls, pard, out='dict'):
		"""Return columns description from pard['table_name']
		The rows are formatted as a list of dictionaries by default.
		"""
		if 'db_table' in pard:
			pard['table_name'] = pard.get('table_name', pard['db_table'])
		pard['fetch_all_row'] = True
		pard['sql'] = "PRAGMA table_info(%(table_name)s)" % pard
		result = cls.send_dict(pard)
		if out == 'dict':
			ll = []
			for rec in result['rows']:
				d = {}
				d['Field'] = rec['name']
				d['Type'] = rec['type']
				d['Null'] = rec['notnull']
				d['Key'] = rec['pk']
				d['Default'] = rec['dflt_value']
				d['Extra'] = ''
				ll.append(d)
			return ll
		elif out == 'list':
			return [rec['name'] for rec in result['rows']]
		
	@classmethod
	def show_index(cls, pard):
		"""
		Return (primary_key_column_list, other_index_description)
		of pard['table_name']
		
		sqlite> pragma index_list(book_episodes);
		seq | name                             | unique
		0   | sqlite_autoindex_book_episodes_1 | 1
		
		sqlite> pragma index_info(sqlite_autoindex_book_episodes_1);
		seqno | cid | name
		0     | 1   | book_id
		1     | 2   | episode_title
		
		"""
		pard['db_table'] = pard['table_name']
		primary_key = cls.get_primary_key(pard)
		pard['sql'] = "pragma index_list(%(table_name)s)" % pard
		result = cls.send(pard)
		index = {}
		for rec in result:
			index_name = rec[1]
			non_unique = 0 if rec[2] else 1
			index[index_name] = {
				'index_name': index_name,
				'columns': [],
				'Non_unique': non_unique
				}
			pard['sql'] = "pragma index_info(%s)" % index_name
			index_info = cls.send(pard)
			for item in index_info:
				index[index_name]['columns'].append(item[2])
		index_list = []
		for key in index.keys():
			index_list.append(index[key])
		return primary_key, index_list

	@classmethod
	def show_create_table(cls, pard):
		pard['sql'] = """
			SELECT sql 
			FROM   sqlite_master
			WHERE  type='table'
			and    name = '%(table_name)s'
			""" % pard
		result = cls.send(pard)
		if result:
			return result[0][0]
		else:
			return ''
