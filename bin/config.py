import tools
import pprint
import os


def get_site_config():
	import yaml
	if 'SITE_CONFIG_YAML' in os.environ:
		buf = open(os.environ['SITE_CONFIG_YAML'], 'r').read()
		site_config = yaml.load(buf)
	else:
		import site_config
		site_config = site_config.config
	return site_config


site_config = get_site_config()


def global_parameters(pard):
	#import site_config
	## Per sicurezza carico da site_config tutte e sole le chiavi che mi 
	## servono
	for k in ['APPSERVER_URI', 'HTTP_PORT', 'MAX_ROW', 'ENABLE_EVAL',
		'AUTH_IP', 'DNS', 'IP_ADDRESS', 'BASE_DIR', 'STORE_PASSWORD',
		'APPLICATION_LOG_FILE', 'DATA_DIR']:
		pard[k] = site_config.get(k)
	
# 	if pard['HTTP_PORT'] != 80:
# 		pard['APPSERVER'] = 'http://' + pard['DNS'] + ':' + str(pard['HTTP_PORT']) + pard['APPSERVER_URI']
# 	else:
# 		pard['APPSERVER'] = 'http://' + pard['DNS'] + pard['APPSERVER_URI']
	
	pard['APPSERVER'] = '/'
	
	# --------------------------------------------------------------- #
	#pard['MONITOR_PORT'] = 8092 Serve solo per medusa ma non lo uso piu'
	pard['BIN_DIR'] = pard['BASE_DIR'] + '/bin'
	pard['LOG_DIR'] = pard['BASE_DIR'] + '/log'
	if not pard['DATA_DIR']:
		pard['DATA_DIR'] = pard['BASE_DIR'] + '/data'
	pard['SID_DIR'] = pard['BASE_DIR'] + '/sid'
	pard['HTML_DIR'] = pard['BASE_DIR'] + '/html'
	pard['IMG_DIR'] = pard['HTML_DIR'] + '/img'
	pard['ICON_DIR'] = pard['HTML_DIR'] + '/icons'
	pard['BACKUP_DIR'] = pard['BASE_DIR'] + '/bck'
	pard['COMMON_LOG_FILE'] = pard['LOG_DIR'] + '/common_log.txt'
	pard['PID_FILE'] = pard['LOG_DIR'] + '/mw.pid'
	pard['SID_LOG_FILE'] = pard['LOG_DIR'] + '/sid_log.txt'
	if not pard['APPLICATION_LOG_FILE']:
		pard['APPLICATION_LOG_FILE'] = pard['LOG_DIR'] + '/application_log.txt'
	pard['BATCH_LOG_FILE'] = pard['LOG_DIR'] + '/batch_log.txt'
	pard['SQL_LOG_FILE'] = pard['LOG_DIR'] + '/sql_log.txt'
	pard['SCGI_LOG_FILE'] = pard['LOG_DIR'] + '/scgi_log.txt'
	pard['LAST_CONN_FILE'] = pard['DATA_DIR'] + '/last_conn.txt'
	pard['PUBLISHING_DIR'] = pard['HTML_DIR']
	pard['DEBUG'] = 1
	pard['SID_TIMEOUT'] = 28800
	pard['DEBUG_SQL'] = 0
	pard['PROJECT'] = 'mw'
	pard['TITLE'] = 'MySQLWeb'
	pard['CSS'] = '<link rel="stylesheet" href="/sqlweb.css" type="text/css">'
	pard['DEFAULT_COLOR'] = '#2a68c8'
	# ------------------------------------------------------------- #
	pard['AUTH_MODULE'] = ['home', 'query', 'db_structure', 'preferences', 'qq']
	return pard


def check_dir_tree(pard):
	for key in ['LOG_DIR', 'DATA_DIR', 'SID_DIR', 'BACKUP_DIR']:
		tools.check_dir(pard[key])


def build_default_config():
	if os.path.isfile('site_config.py'):
		pass
	else:
		print 'Creating default site configuration. . .',
		d = dict(
			IP_ADDRESS = '127.0.0.1',
			DNS = 'localhost',
			BASE_DIR = '..',
			APPSERVER_URI = '',
			HTTP_PORT = 8082,
			AUTH_IP = ['127.0.0.1'],
			MAX_ROW = 50,
			STORE_PASSWORD = False
			)
		buf = 'config = ' + pprint.pformat(d)
		open('site_config.py', 'w').write(buf)
		print 'done!'


if __name__ == '__main__':
	#build_default_config()
	get_site_config({})