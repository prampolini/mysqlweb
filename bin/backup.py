import commands
import time
import os


def backup_sorgenti(pard):
	"""Esegue il backup dei sorgenti del progetto e della
	directory html"""
	pard['now'] = time.strftime('%Y%m%d', time.localtime(time.time()))
	cmd = 'cd %(BASE_DIR)s; /usr/bin/tar czvf %(BACKUP_DIR)s/%(PROJECT)s_%(now)s.tgz lighttpd_scgi_mw.conf bin/*.py bin/mysqlweb-server html' % pard
	(status, output) = commands.getstatusoutput(cmd)
	print 'Output --> %s' % output
	print 'Comando --> %s' % cmd
	print 'Stato --> %s' % status



if __name__ == '__main__':
	import config
	pard = config.global_parameters(pard={})
	backup_sorgenti(pard)
