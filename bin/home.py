import string

import auth
import menu


######################################################################
###                   MAIN
######################################################################

# def build_home(pard):
# 	pard['sql'] = "select codice_atleta from atleti" % pard
# 	result = db.db_access.mysql_dict(pard)
# 	
# 	h = ['<table border="1"><tr><th>Lista</th></tr>']
# 	for rec in result:
# 		h.append('<tr><td>%(codice_atleta)s</td></tr>' % rec)
# 	h.append('</table>')
# 	pard['tab_html'] = '\n'.join(h)
# 	
# 	
# 	pard['html'] = """
# 	<html>
# 	<head>
# 	<title>%(TITLE)s</title>
# 	</head>
# 	<body>
# 	<center>
# 	%(tab_html)s
# 	</center>
# 	</body>
# 	</html>
# 	""" % pard
# 
# 	return pard


def build_not_authorized(pard):
	pard = auth.load_sid_data(pard)
	pard['module'] = 'home'
	pard['program'] = 'build_home'
	try:
		pard['menu'] = menu.menu_bar(pard)
		pard['msg'] = 'Utente non autorizzato!'
	except:
		pard['menu'] = '<a href="%(APPSERVER)s?module=home&program=build_home&sid=%(sid)s">Home Page</a>' % pard
		pard['msg'] = "La pagina richiesta non e' disponibile!"
	html = """
	<html>
	%(CSS)s
	<head>
	<title>%(TITLE)s</title>
	</head>
	<body>
	<center>
	%(menu)s
	<h3>%(msg)s</h3>
	</center>
	</body>
	</html>
	"""
	pard['html'] = html % pard
	return pard


def lock(pard):
	#pard['menu'] = menu.menu_bar(pard)
	html = """
	<html>
	<head>
	<title>%(TITLE)s</title>
	</head>
	<body>
	<center>
	<br>
	<img src="/img/progress.gif" border=0>
	<h3>Sito in Manutenzione!</h3>
	</center>
	</body>
	</html>
	"""
	pard['html'] = html % pard
	return pard


def error_manager(pard):
	pard['menu'] = menu.menu_bar(pard)
	if pard['ERROR'] == 'MISSING_SID':
		pard['error_message'] = '<b>Pagina non trovata</b>'
	else:
		pard['error_message'] = "<b>Si e' verificato un errore</b>"
	errtext = string.split(pard['ERROR'], '\n')[-2]
	errtext = string.replace(errtext, '<B>', '')
	errtext = string.replace(errtext, '</B>', '')
	pard['errtext'] = errtext
	pard['html'] = """
		<html>
		<head>
		%(CSS)s
		<title>%(TITLE)s - ERROR</title>
		</head>
		<center>
		%(menu)s
		<body>	
			<!--
			%(ERROR)s
			-->
			<br><br>
			<table align="center" width="600" height="200" bgcolor="white" bordercolor="#DDDDDD" border=0>
				<tr>
					<td align="center"><br><img src="/img/error.gif"><br><br></td>
				</tr>
				<tr>
					<td align="center">%(error_message)s<br>
					<br>
					<b>%(errtext)s</b><br><br>
					</td>
				</tr>
				<tr>
					<td align="center"><a href="javascript:history.go(-1)">Indietro</a><br><br></td>
				</tr>
			</table>
		</body>
		</center>
		</html>
	""" % pard
	return pard



