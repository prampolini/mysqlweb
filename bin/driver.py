from sys import exc_info

import traceback
import string
import time
import cgi
import sys

import config
import auth
import tools


def build_exception(type=None, value=None, tb=None, limit=None):
	if type is None:
		type, value, tb = exc_info()
	buf = "<H3>Traceback (most recent call last):</H3>"
	list = traceback.format_tb(tb, limit) + traceback.format_exception_only(type, value)
	buf = buf + "<PRE>%s<B>%s</B></PRE>" % (escape("".join(list[:-1])), escape(list[-1]))
	del tb
	return buf


def escape(s, quote=None):
	s = s.replace("&", "&amp;") # Must be done first!
	s = s.replace("<", "&lt;")
	s = s.replace(">", "&gt;")
	if quote:
		s = s.replace('"', "&quot;")
	return s


def log_hit(start, stop, ip, id_utente, command, bytes, error, pard):
	try:
		action = pard.get('action', '')
		id_utente = pard.get('sid_user', '')
		h = pard.get('html', '')
		bytes = len(h)
		exec_time = repr(stop - start)[0:5]
		if pard['APPLICATION_LOG_FILE'] == 'stdout':
			log_file = sys.stdout
		else:
			log_file = open(pard['APPLICATION_LOG_FILE'], 'a')
		log_date = time.asctime(time.localtime(time.time()))
		error_id = ''
		if error != '':
			error_id = string.split(repr(stop), '.')[0]
			error = string.replace(error[0], '\n', '\040')
			error = 'ERROR_FILE -> ' + error_id + '.err'
		log_buf = string.join([log_date, id_utente, ip, repr(bytes), exec_time, command, action, pard['HTTP_REFERER'], error], '\t') + '\n'
		log_file.write(log_buf)
		if pard['APPLICATION_LOG_FILE'] != 'stdout':
			log_file.close()
		if error_id != '':
			error_file = pard['LOG_DIR'] + '/' + error_id + '.err'
			fp = open(error_file, 'w')
			fp.write(error + '\n')
			keys = pard.keys()
			keys.sort()
			for k in keys:
				buf = repr(k) + ': ' + repr(pard[k]) + '\n'
				fp.write(buf)
			fp.close()
		#auth.log_hit(log_buf, pard)
		return ''
	except:
		return build_exception()


def log_sid(start, stop, bytes, pard):
	if pard.has_key('sid'):
		try:
			exec_time = repr(stop - start)[0:5]
			sid_file = auth.sid_get(pard)
			log_date = time.asctime(time.localtime(time.time()))
			pard['EXEC_TIME'] = exec_time
			pard['LOG_DATE'] = log_date
			pard['BYTES'] = repr(bytes)
			sid_file['sid_buffer'] = pard.get('sid_buffer', '')
			result = auth.sid_set(pard, sid_file)
			return ''
		except:
			return build_exception()


def reload_developing_modules(pard):
	try:
		buf = open('%(BIN_DIR)s/developing_modules' % pard, 'r').read()
	except IOError:
		return []
	modules_list = buf.split()
	for item in modules_list:
		cmd = 'import %s;reload(%s)' % (item, item) 
		exec(cmd)
	return modules_list


def driver(pard):
	start = time.time()
	html = ''
	error_log = ''
	error_sid = ''
	command = ''
	id_utente = ''
	result = {'html': '', 'header': 'Content-type: text/html\n\n'}
	pard['header'] = pard.get('header', 'Content-type: text/html\n\n')

	try:
		pard = config.global_parameters(pard)
		if 'module' in pard and pard['module'] == 'preferences':
			pard['flag_sid'] = 'OK'
			pard['sid'] = 'preferences'
		else:
			pard = auth.check_sid(pard)
		if pard['flag_sid'] == 'NEW':
			pard['tmp_sid'] = auth.calc_sid(pard)
			pard['module'] = 'auth'
			pard['program'] = 'login'
		
		if pard['flag_sid'] == 'KO':
			flag = 'stop'
		else:
			pard['program'] = pard.get('program', 'main')
			command = pard['module'] + '.' + pard['program'] + '(pard)'
			if command in ['auth.login(pard)', 'auth.check_authentication(pard)']:
				flag = 'LOGIN'
			else:
				pard = auth.load_sid_data(pard)
				if pard['module'] in pard['AUTH_MODULE']:
					flag = 'GO'
				else:
					flag = 'STOP'
		
		if flag in ('GO', 'LOGIN'):
			if pard['DEBUG'] == 1:
				reload_developing_modules(pard)
			if __import__(pard['module']).__dict__.has_key(pard['program']):
				pard = __import__(pard['module']).__dict__[pard['program']](pard)
		else:
			pard = auth.build_not_authorized(pard)
		
		stop = time.time()
		pard['sid_user'] = pard.get('sid_user', '')
		error_log = log_hit(start, stop, pard['REMOTE_ADDR'], pard['sid_user'], command, len(pard['html']), pard['ERROR'], pard)
		error_sid = log_sid(start, stop, len(html), pard)
		
		#output.write(pard['header'])
		if pard['header'] == 'Content-type: text/html\n\n':
			pard['html'] = pard['html'].replace('\t', '')
# 		else:
# 			for item in pard['header']:
# 				request.setHeader(item[0], item[1])
		
		if error_log:
			print error_log
		if error_sid:
			print error_sid
		
		if isinstance(pard['html'], unicode):
			result['html'] = pard['html'].encode('utf8')
		else:
			result['html'] = pard['html']
		result['header'] = pard['header']

	except:
		stop = time.time()
		pard['ERROR'] = build_exception()
		try:
			pard = auth.error_manager(pard)
			pard['html'] = string.replace(pard['html'], '\t', '')
			#output.write(pard['header'])
			result['html'] = pard['html']
		except:
			#output.write(pard['header'])
			result['html'] = pard['ERROR']
		
		error_log = log_hit(start, stop, pard['REMOTE_ADDR'], id_utente, command, len(html), pard['ERROR'], pard)
		error_sid = log_sid(start, stop, len(html), pard)
		
		if error_log:
			print error_log
		if error_sid:
			print error_sid
	
	exec_time = repr(stop - start)[0:5]
	#output.write('<br><br><center><font size=1><exec_time>Exec Time: ' + exec_time + '</exec_time></font></center>')
	return result
