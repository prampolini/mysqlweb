import string
import tools


def menu_bar(pard):
	testata = '&nbsp;&nbsp;<trad>Utente:<trad>&nbsp;' + pard['sid_descrizione'] + '&nbsp;&nbsp;<trad>Gruppo:</trad>&nbsp;'
	#spaziatore = '<br>' + ('&nbsp;' * 13)
	spaziatore = ',&nbsp;'
	#table_menu = '<table border=0 width=700 style="border-collapse: collapse">'
	table_menu = '<table border=0 cellspacing=0 width=700>'
	t = 0
	for gruppo in pard['sid_gruppi_appartenenza']:
		if pard['sid_gruppi_appartenenza'].index(gruppo) == len(pard['sid_gruppi_appartenenza']) -1:
			testata = testata + gruppo
		else:
			testata = testata + gruppo + spaziatore
		if t >= 1:
			break
		t = t + 1
	if len(pard['sid_gruppi_appartenenza']) > 1:
		testata = string.replace(testata, 'Gruppo:', 'Gruppi:')
	pard['testata'] = testata
	html = [table_menu]
	html.append('<tr>')
	html.append('<td class=menu_head colspan=9 height=40>%(testata)s</td>')
	#html.append('<td class=menu_head width=30><a href="http://www.python.org"><img src="/img/logo_menu.png" border=0></a></td>')
	#html.append('<td class=menu_head width=250 align="right"><font face="Times New Roman,times,verdana,geneva,helvetica" color="white" size="+2"><b>M A S T E R uninuoto&nbsp;</b></font></td>')
	html.append('<td class=menu_head width=250 align="right"><font face="Times New Roman,times,verdana,geneva,helvetica" color="white" size="+2"><b>&nbsp;</b></font></td>')
	html.append('</tr>')
	html.append('</table>')
	
	### menu ###
	html.append(table_menu)
	html.append('<tr><td class=menu_sub>')
	riga = []
	flag_home = 0
	pard['menu_operation_tab'][-1]['last'] = 'current'
	for item in pard['menu_operation_tab']:
		item['APPSERVER'] = pard['APPSERVER']
		item['sid'] = pard['sid']
		if item['program'] == 'build_home':
			flag_home = 1
			riga.append('<a class=menu href="%(APPSERVER)s?module=home&program=build_home&sid=%(sid)s">M A S T E R</a>' % item)
		#elif item.has_key('last'):
		#	riga.append('<span class=menucurrent>%(desc)s</span>' % item)
		else:
			riga.append('<a  class=menu href="%(APPSERVER)s?module=%(module)s&program=%(program)s&sid=%(sid)s">%(desc)s</a>' % item)
	if not flag_home:
		riga.insert(0,'<a class=menu href="%(APPSERVER)s?module=home&program=build_home&sid=%(sid)s">M A S T E R</a>' % item)
	#riga = string.join(riga, '&nbsp;&nbsp;&nbsp;<img src="/img/arrow_yellow.gif" border=0>&nbsp;&nbsp;&nbsp;')
	riga = string.join(riga, '&nbsp;&nbsp;::&nbsp;&nbsp;')
	
	html.append(riga)
	html.append('</td></tr>')
	html.append('</table>')
	html.append('<br>')
	html = string.join(html, '\n')
	html = html % pard
	return html


def site_map(pard, menu='',level=0):
	if menu == '':
		menu = pard['MENU']
	menu_keys = menu.keys()
	menu_keys.sort()
	h = []
	if level == 0:
		h.append('<table width=600 cellpadding=4>')
	for k in menu_keys:
		pard['module'] = menu[k]['module']
		pard['program'] = menu[k]['program']
		pard['desc'] = menu[k]['desc']
		if level == 0:
			pard['stile'] = 'sitemapb'
		else:
			pard['stile'] = 'sitemap'
		if pard['module'] == '':
			continue
		if pard['DISPLAY_ALL_FUNCTION'] == 'no':
			if k[1] not in pard['sid_auth_function_list']:
				continue
		pard['spazi'] = '&nbsp;' * 6 * level
		h.append('<tr><td>%(spazi)s<a class=%(stile)s href="%(APPSERVER)s?module=%(module)s&program=%(program)s&sid=%(sid)s"><img src="/img/blue_arrow.gif" border=0>&nbsp;%(desc)s</a></td></tr>' % pard)
		if menu[k].has_key('menu'):
			if menu[k]['menu']:
				h.append(site_map(pard, menu[k]['menu'], level+1))
	if level == 0:
		h.append('</table>')
	h = string.join(h, '\n')
	return h


def menu_map(pard, menu=''):
	menu_cur = pard['menu_operation_tab'][-1]['desc']
	if menu == '':
		menu = pard['MENU']
	for k in menu.keys():
		if menu_cur == k[1]:
			return site_map(pard, menu[k]['menu'])
		else:
			if menu[k].has_key('menu'):
				if menu[k]['menu']:
					result = menu_map(pard, menu[k]['menu'])
					if result:
						return result
	return ''


