#
#  Web Query Browser
#
#  Author: gianfranco
#
#  || ToDo ||
#  - Gestire errori in update (per adesso va bene cosi')
#  - Pagina HTML con Query History (Aggiungere filtro?)
#  - Mettere un div Database structure (forse e' comodo cosi'?)
#  - Preferenze
#    - 30 righe limit
#  - Pacchettizzare
#  

import time
import os
import re
import pprint

import cjson

#import db_access
import db
import auth
import sql_info
import tools

def main(pard):
	if pard['module'] == 'auth' and pard['program'] == 'check_authentication':
		pard['set_cookie'] = True
	else:
		pard['set_cookie'] = False
	pard['module'] = 'query'
	pard['program'] = 'main'
	pard['action'] = pard.get('action', 'start')
	pard['menu'] = render_menu(pard)
	pard['search_form'] = ''
	pard['msg'] = []
	pard['result_msg'] = ''
	pard['main_body'] = ''
	pard['javascript'] = '<script src="/javascripts/prototype.js" type="text/javascript"></script>\n'
	pard['javascript'] += ajax_js(pard)
	pard['opensearch'] = opensearch(pard)
	if pard['set_cookie']:
		pard['javascript'] += auth.js_create_cookie(pard)
		
	# --------------------------- start ----------------------------
	if pard['action'] == 'start':
		pard['search_form'] = render_search_form(pard)	
		pard['main_body'] = """<p>MySQL Query Navigator</p>"""

	# --------------------------- run_query ----------------------------
	elif pard['action'] in ('run_query', 'fetchall'):
		pard['query'] = pard['query'].lstrip()
		dec = re.compile('^\d+.?\d*$')
		pard['sql_query'] = pard['query']
		pard['sql_info'] = sql_info.get_sql_info(pard)
		if pard['sql_info']['query_command'] == 'wsel':
			pard = wsel_query_wizard(pard)
		elif pard['sql_info']['query_command'] == 'wins':
			pard = wins_query_wizard(pard)
		elif pard['sql_info']['query_command'] == 'silk':
			pard = silk_icons(pard)
# 		elif pard['sql_info']['query_command'] == 'eval':
# 			pard = eval_query(pard)
		elif dec.findall(pard['sql_info']['query_command']):
			pard['sql'] = 'select ' + pard['query']
			pard = exec_query(pard)
		else:
			pard['sql'] = clean_query(pard['query'])
			pard = exec_query(pard)
	
	# --------------------------- run_query2 ----------------------------
	elif pard['action'] in ('run_query2', 'run_procedure'):
		pard['sqls'] = split_query(pard['query2'])
		if pard['action'] == 'run_query2':
			pard['sql_query'] = pard['sqls'][-1]
			pard['sql_info'] = sql_info.get_sql_info(pard)
			pard['sql'] = pard['sqls'][-1]
			pard['query'] = pard['sqls'][-1]
			pard = exec_query(pard)
		else:
			#pard['main_body'] = '<pre>%s</pre>' % pprint.pformat(run_procedure(pard))
			pard = run_procedure(pard)

	# --------------------------- row_delete ----------------------------
	elif pard['action'] == 'row_delete':
		pard['msg'].append(delete_row(pard))
		pard['query'] = pard['query'].lstrip()
		pard['sql_query'] = pard['query']
		pard['sql_info'] = sql_info.get_sql_info(pard)
		pard['sql'] = clean_query(pard['query'])
		pard['action'] = pard['prev_action']
		pard = exec_query(pard)
	
	# --------------------------- allow_edit_field ----------------------------
	elif pard['action'] == 'allow_edit_field':
		pard['field_value'] = db_get_field_value(pard)
		pard['html'] = render_edit_field(pard)
		return pard

	# --------------------------- restore_field ----------------------------
	elif pard['action'] == 'restore_field':
		pard['html'] = db_get_field_value(pard)
		return pard

	# --------------------------- update_field ----------------------------
	elif pard['action'] == 'update_field':
		dummy = db_update_field(pard)
		pard['html'] = db_get_field_value(pard)
		return pard
	
	# --------------------------- query_history ----------------------------
	elif pard['action'] == 'query_history':
		pard['search_form'] = render_search_form(pard)
		pard['history'] = get_query_history(pard)
		pard['main_body'] = render_query_history(pard)
	
	# --------------------------- prepare_insert ----------------------------
	elif pard['action'] == 'prepare_insert':
		pard['html'] = prepare_insert(pard)
		return pard


	
	else:
		pard['result_msg'] = tools.format_messaggio('Function not available. ("%(action)s")' % pard)
		pard['main_body'] = '<input type="button" name="submit_form" value="Back" onclick="javascript:history.go(-1);">'
	
	pard['html'] = render_page(pard)
	return pard


def run_procedure(pard):
	query = []
	for pard['sql_query'] in pard['sqls']:
		qd = {}
		qd['query'] = pard['sql_query']
		qd['sql_info'] = sql_info.get_sql_info(pard)
		query.append(qd)
	
	pard['sqls'] = query
	pard['query'] = query[-1]['query']	
	result = db.db_access.run_procedure(pard)
	html = []
	ind = 1
	pard['result'] = []
	for resd in result:
		resd['num_row'] = len(resd['rows'])
		resd['ind'] = str(ind)
		if resd['error']:
			html.append('%(ind)s - <span class="err">%(error)s</span>' % resd)
			pard['query'] = resd['query']
		elif resd['num_row'] > 0:
			if pard.has_key('limit_clause'):
				resd['limit_msg'] = ' - "limit %(MAX_ROW)s" clause has been added!' % pard
			else:
				resd['limit_msg'] = ''
			html.append('%(ind)s - Result <b>1</b> - <b>%(num_row)s</b> of <b>%(rowcount)s</b> (<b>%(exec_time)s</b> seconds) %(limit_msg)s' % resd)
			pard['result'] = resd
		elif resd['rowcount'] > 0:
			html.append('%(ind)s - Affected row: <b>%(rowcount)s</b> (<b>%(exec_time)s</b> seconds)' % resd)
		else:
			html.append('%(ind)s - Query OK, <b>Empty set</b> (<b>%(exec_time)s</b> seconds)' % resd)
		ind += 1
	
	pard['msg'] = '<br>'.join(html)
	
	pard['result_msg'] = render_result_msg(pard)
	if pard['result']:
		pard['main_body'] = render_result(pard)
	pard['search_form'] = render_search_form(pard)
	
	return pard


def exec_query(pard):
	"""Exec the query and sets:
		pard['search_form'],
		pard['result_msg'],
		pard['main_body'],
		pard['javascript']
	"""	
	try:
		if (pard['action'] == 'fetchall'
		or  pard['sql_info']['query_command'] != 'select'):
			pard['MAX_ROW'] = 'fetchall'
# 		else:
# 			if (pard['sql_info']['query_command'] == 'select'
# 			and 'limit' not in pard['sql_info']['words']):
# 				pard['sql'] = pard['sql'] + '\nlimit %(MAX_ROW)s' % pard
# 				pard['limit_clause'] = True
		pard['result'] = db.db_access.send_dict(pard)
		set_query_history(pard)
		pard['num_row'] = len(pard['result']['rows'])
		pard['rowcount'] = pard['result']['rowcount']
		pard['exec_time'] = pard['result']['exec_time']
		
		pard['update_allowed'] = False
		if pard['sql_info']['query_command'] == 'select':
			if 'select_tables_list' in pard['sql_info']:
				if len(pard['sql_info']['select_tables_list']) == 1:
					primary_key = set(pard['sql_info']['primary_key'])
					selected_columns = set(pard['result']['c_name'])
					if primary_key.issubset(selected_columns):
						if primary_key:
							pard['update_allowed'] = True
		
		if pard['num_row'] > 0:
			pard['query_result'] = 'Result <b>1</b> - <b>%(num_row)s</b> of <b>%(rowcount)s</b> (<b>%(exec_time)s</b> seconds)' % pard
		elif pard['rowcount'] > 0:
			pard['query_result'] = 'Affected row: <b>%(rowcount)s</b> (<b>%(exec_time)s</b> seconds)' % pard
		else:
			pard['query_result'] = 'Query OK, <b>Empty set</b> (<b>%(exec_time)s</b> seconds)' % pard
		
		if pard.has_key('limit_clause'):
			pard['query_result'] = pard['query_result'] + ' - "limit %(MAX_ROW)s" clause has been added!' % pard
		
		pard['edit_data'] = pard.get('edit_data', '')
		if pard['update_allowed']:
			if pard['edit_data'] == 'True':
				pard['update_img'] = """
					<img src="/img/lock_open.png" border="0"
					   title="Read only"
					   onClick="document.getElementById('edit_data').value=''; document.forms['query'].submit();">&nbsp;
					   """ % pard
			else:
				pard['update_img'] = """
					<img src="/img/lock.png" border="0"
					   title="Edit Data"
					   onClick="document.getElementById('edit_data').value='True'; document.forms['query'].submit();">&nbsp;
					   """ % pard
		else:
			pard['update_img'] = ''
		pard['javascript'] = pard['javascript'] + render_query_info(pard)

		pard['msg'].append("""
			<img src="/img/view.png" title="Query Information" 
			 onClick="display_query_info();">
			 <div id="query_info" class="html_title" style="width: 400px;"></div>
			 &nbsp;&nbsp;%(update_img)s&nbsp;&nbsp;
			 %(query_result)s
			 """ % pard)
		
		pard['result_msg'] = render_result_msg(pard)
		pard['main_body'] = render_result(pard)
	except db.db_access.DatabaseError, (errno, strerror):
		pard['msg'] = '<span class="err">DatabaseError: %s - %s</span>' % (str(errno), strerror)
		pard['result_msg'] = render_result_msg(pard)
	pard['search_form'] = render_search_form(pard)
	
	return pard


def split_query(sql):
	ll = sql.split(';\r\n')
	sqls = []
	for x in ll:
		x = x.strip()
		if x:
			sqls.append(clean_query(x))
	return sqls


def clean_query(sql):
	"""
	Eseguiamo tutti i controlli sull'sql inserito.
	"""
	# Rimozione dell'eventuale ";" inserito a fine riga
	sql = sql.strip()
	if sql[-1:] == ';':
		sql = sql[:-1]
	
	return sql


def render_search_form(pard):
	pard['query'] = pard.get('query', '')
	pard['query2'] = pard.get('query2', '')
	pard['edit_data'] = pard.get('edit_data', '')
# 	pard['btn_run'] = """
# 		<input type="submit" name="submit_form" value="Run" class="btn"
# 		onclick='action.value="run_query"; form.submit();'
# 		accesskey="r" />
# 		"""
	pard['javascript'] = pard['javascript'] + """
		<script type="text/javascript" language="javascript" charset="utf-8">
		function runquery(action)
			{
			document.getElementById('edit_data').value='';
			document.forms['query'].elements['action'].value=action;
			document.forms['query'].submit();
			}

		function run_selection()
			{
			var selected_query = getSelection($('query2'));
			$('query').value = selected_query;
			runquery('run_query');
			}
			
		function textarea_add_row(inc, tarea)
			{
			var tag_h = tarea + '_area_height';
			var tag_td = 'td_' + tarea;
			document.getElementById(tag_h).value = parseInt(document.getElementById(tag_td).style.height) + inc;
			document.getElementById(tag_td).style.height = document.getElementById(tag_h).value;
			}
		
		function db_structure()
			{
				newWindow = window.open("%(APPSERVER)s?module=db_structure&sid=%(sid)s",
							"db_structure_%(DB)s",
							config="height=600, width=800, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no"
							);
				newWindow.focus();
			 }
		
		function toggle_multi_area()
		{
			if ($('query2_display').value == 'N')
			{
				$('query2_display').value = 'S';
			}
			else
			{
				$('query2_display').value = 'N';
			}
			$('multi_area').toggle();
		}
				
		</script>
		""" % pard
	pard['btn_run'] ="""
		<a href="javascript:runquery('run_query')"
		title="Run query"
		accesskey="r">
		<u>R</u>un</a>"""
	pard['btn_fetchall'] ="""
		<a href="javascript:runquery('fetchall')"
		title="Fetch all record"
		accesskey="a">
		Fetch <u>A</u>ll</a>"""
	pard['btn_next'] = """
		<img src="/img/forward.png" border="0"
		title="Next query"
		onClick="navhistory('next')">
		"""
	pard['btn_prev'] = """
		<img src="/img/back.png" border="0"
		title="Previus query"
		onClick="navhistory('prev')">
		"""
	pard['btn_history'] = """
		<a href="%(APPSERVER)s?module=query&action=query_history&sid=%(sid)s"
		title="History"
		accesskey="h">
		<img src="/img/History.png" border="0"></a>
		""" % pard
	pard['link_new_query'] = """
		<a href="%(APPSERVER)s?module=query&action=start&sid=%(sid)s"
		title="New Query"
		accesskey="n">
		<u>N</u>ew</a>""" % pard
	pard['btn_textarea_add_row'] = """
		<img src="/img/textarea_add_row.png" border="0"
		title="Increase query area"
		onClick="textarea_add_row(20, 'query')">
		"""
	pard['btn_textarea_remove_row'] = """
		<img src="/img/textarea_remove_row.png" border="0"
		title="Decrease query area"
		onClick="textarea_add_row(-20, 'query')">
		"""
	pard['btn_db_structure'] = """
		<img src="/img/database_gear.png" border="0"
		title="DB Structure"
		onClick="db_structure();">
		"""
	pard['btn_multi_area'] = """
		<img src="/icons/page_white_put.png" border="0"
		title="Multi Query"
		onClick="toggle_multi_area();">
		"""
	pard['btn_textarea2_add_row'] = """
		<img src="/img/textarea_add_row.png" border="0"
		title="Increase query area"
		onClick="textarea_add_row(20, 'query2')">
		"""
	pard['btn_textarea2_remove_row'] = """
		<img src="/img/textarea_remove_row.png" border="0"
		title="Decrease query area"
		onClick="textarea_add_row(-20, 'query2')">
		"""
	pard['btn_run2'] ="""
		<img src="/icons/lightning.png" border="0"
		title="Run this query"
		onClick="runquery('run_query2');">
		"""
	pard['btn_run_selection'] ="""
		<img src="/icons/lightning_go.png" border="0"
		title="Run Selection"
		onClick="run_selection();">
		"""
	pard['btn_exec_all'] ="""
		<a href="javascript:runquery('run_procedure')"
		title="Exec Procedure">Exec All</a>"""
	
	
	pard['javascript'] = pard['javascript'] + js_navhistory(pard)
	pard['query_history_len'] = str(len(pard['query_history']) - 1)
	pard['query_area_height'] = pard.get('query_area_height', '75')
	pard['query2_area_height'] = pard.get('query2_area_height', '75')
	pard['query2_display'] = pard.get('query2_display', 'N')
	if pard['query2_display'] == 'N':
		pard['m_style'] = 'style="display: none;"'
	else:
		pard['m_style'] = ''
	h = """
	<input type="hidden" name="edit_data" id="edit_data" value="%(edit_data)s" />
	<input type="hidden" name="query_area_height" id="query_area_height" value="%(query_area_height)s" />
	<input type="hidden" name="pk_del" id="pk_del" value="" />
	<input type="hidden" name="tbl_del" id="tbl_del" value="" />
	<input type="hidden" name="prev_action" id="prev_action" value="%(action)s" />
	<table width="100%%">
	<tr>
		
		<td style="margin: 0; padding: 0; height: %(query_area_height)spx;" id="td_query">
			<textarea style="border: none; width: 100%%; height: 100%%" id="query" name="query">%(query)s</textarea>
		</td>
				
		<td width="130px" style="margin: 0; padding: 0; vertical-align: top; background-color: #DDDDDD;">
		
			<table class="action" width="100%%">
			<tr>
				<td>%(btn_db_structure)s</td>
				<td>%(btn_run)s</td>
				<td>%(link_new_query)s</td>
			</tr>
			<tr>
				<td style="vertical-align: bottom;">
					%(btn_textarea_remove_row)s
				</td>
				<td colspan="2">
					%(btn_fetchall)s
				</td>
			</tr>
			<tr>
				<td style="vertical-align: bottom;">
					%(btn_textarea_add_row)s
				</td>
				<td>
					<input type="hidden" name="query_index" id="query_index" value="%(query_history_len)s" />
					%(btn_prev)s%(btn_next)s%(btn_history)s
				</td>
				<td>
					%(btn_multi_area)s
				</td>
			</tr>
			</table>
		
		</td>
	</tr>
	</table>
	<script type="text/javascript" language="javascript" charset="utf-8">
		document.getElementById('query').focus();
	</script>
	
	<div id="multi_area" %(m_style)s>
	
	<input type="hidden" name="query2_display" id="query2_display" value="%(query2_display)s" />
	<input type="hidden" name="query2_area_height" id="query2_area_height" value="%(query2_area_height)s" />
	
	<table width="100%%">
	<tr>
		
		<td style="margin: 0; padding: 0; height: %(query2_area_height)spx;" id="td_query2">
			<textarea style="border: none; width: 100%%; height: 100%%" id="query2" name="query2">%(query2)s</textarea>
		</td>
				
		<td width="130px" style="margin: 0; padding: 0; vertical-align: top; background-color: #DDDDDD;">
		
			<table class="action" width="100%%">
			<tr>
				<td style="vertical-align: bottom;">
					%(btn_textarea2_remove_row)s
				</td>
				<td style="vertical-align: bottom;">%(btn_run2)s</td>
				<td>%(btn_run_selection)s</td>				
			</tr>
			<tr>
				<td style="vertical-align: bottom;">
					%(btn_textarea2_add_row)s
				</td>
				<td colspan="2">%(btn_exec_all)s</td>
			</tr>
			</table>	
		</td>
	</tr>
	</table>
	
	</div><!-- id="multi_area" -->
	
	""" % pard
	return h


def render_result_msg(pard):
	pard['msg'] = pard.get('msg', '')
	if isinstance(pard['msg'], list):
		pard['msg'] = '<br>'.join(pard['msg'])
	h = """
		<table width="100%%">
			<tr>
			<td bgcolor="#e5ecf9" align="left" nowrap style="border-top: 1px solid #3366CC;">
			%(msg)s
			</td>
			</tr>
		</table>
		""" % pard	
	return h


def render_result(pard):
	pard['update_allowed'] = pard.get('update_allowed', False)
	h = []
	if pard['result']['rows']:
		h.append('<table width=100%%>')
		h.append('<tr>')
		h.append('<th scope="col">&nbsp;</th>')
		for col in pard['result']['c_name']:
			h.append('<th scope="col">%s</th>' % col)
		h.append('</tr>')
		
		if pard['update_allowed'] and pard['edit_data'] == 'True':
			h.append(render_result_for_update(pard))
		else:
			row_num = 1
			for rec in pard['result']['rows']:
				rec = tools.sbianca(rec)
				h.append('<tr valign="top">')
				h.append('<th style="text-align: right; width: 20px;">%s</th>' % str(row_num))
				for key in pard['result']['c_name']:
					if str(rec[key]).find('\n') > -1:
						rec[key] = '<pre>%s</pre>' % str(rec[key])
					h.append('<td>%s</td>' % str(rec[key]))
				h.append('</tr>')
				row_num += 1
		h.append('</table>')
	h = '\n'.join(h) 	
	return h


def render_result_for_update(pard):
# 	pard_js_action =  '{module: "query",'
# 	pard_js_action += 'action: "allow_edit_field",'
# 	pard_js_action += 'appserver: "%(APPSERVER)s",'
# 	pard_js_action += 'sid: "%(sid)s",'
# 	pard_js_action += 'table_name: "%s",' % pard['sql_info']['select_tables_list'][0]
# 	pard_js_action = pard_js_action % pard
	
	pard['table_name'] = pard['sql_info']['select_tables_list'][0]
	pard['javascript'] += ajax_allow_edit_field(pard)
	h = []
	row_num = 1
	for rec in pard['result']['rows']:
		pk_dict = {}
		
		# encode primary key in a json object
		for key in pard['sql_info']['primary_key']:
			pk_dict[key] = rec[key]
		pk_json = cjson.encode(pk_dict)
		
		rec = tools.sbianca(rec)
		h.append('<tr valign="top">')
		d = {'pk': pk_json,
			 'row_num': str(row_num),
			 'table_name': cjson.encode(pard['table_name'])}
		h.append("""
			<th nowrap style="text-align: right; width: 30px;">
				%(row_num)s&nbsp;
				<img src="/icons/delete.png"
					title="Delete"
					onClick='row_delete({pk: %(pk)s, table_name: %(table_name)s})'
					style="border: solid 0px;">
				&nbsp;
				<img src="/icons/add.png"
					title="Duplicate this record"
					onClick='prepare_insert({pk: %(pk)s, table_name: %(table_name)s})'
					style="border: solid 0px;">
			</th>
			""" % d)
		col_num = 1
		for key in pard['result']['c_name']:
			if str(rec[key]).find('\n') > -1:
				rec[key] = '<pre>%s</pre>' % str(rec[key])
			if key in pard['sql_info']['primary_key'] or key not in pard['sql_info']['table_columns']:
				h.append('<td>%s</td>' % str(rec[key]))
			else:
				#pard_js = pard_js_action
				pard_js = '{pk: %s,' % pk_json
				pard_js += 'area: "r%sc%s",' % (str(row_num), str(col_num))
				pard_js += 'field_name: "%s"}' % key
				h.append("""<td onDblClick='allow_edit_field(%s)'><span id="r%sc%s">%s</span></td>""" % (pard_js, str(row_num), str(col_num), str(rec[key])) )
			col_num += 1
		h.append('</tr>')
		row_num += 1
	h = '\n'.join(h)
	return h


def render_query_info(pard):
	html = ["""
		<table style="width: 100%;">
		<tr>
		<th colspan="2">
			<img src="/img/uncheck.png" border="0" onClick="document.getElementById('query_info').innerHTML = '';">
			&nbsp;&nbsp;&nbsp;Query Information
		</th>
		</tr>
		"""]
	for key in pard['sql_info'].keys():
		if key == 'words':
			continue
		elif isinstance(pard['sql_info'][key], list):
			html.append('<tr><td><b>%s:&nbsp;</b></td><td>%s</td></tr>' % (key, ', '.join(pard['sql_info'][key])))
		else:
			html.append("<tr><td><b>%s:&nbsp;</b></td><td>%s</td></tr>" % (key, str(pard['sql_info'][key])))
	html.append('</table>')
	html = '\n'.join(html)
	html = tools.escape_javascript(html)
	js = """
		<script type="text/javascript" language="javascript" charset="utf-8">
		
		function display_query_info()
		{
			content = '%s';
			document.getElementById('query_info').innerHTML = content;
		}
		
		</script>
		""" % html
	return js


def opensearch(pard):
	html = """
		<link title="MySQL Query Navigator: %(ALIAS)s" rel="search" type="application/opensearchdescription+xml" 
		href="%(APPSERVER)s/OpenSearch/%(ALIAS)s.xml">
		""" % pard
	return html


def render_page(pard):
	html = """
		<html>
		<head>
		%(CSS)s
		<title>%(TITLE)s</title>
		%(javascript)s
		%(opensearch)s
		</head>
		<body>
		%(menu)s
		<form action="%(APPSERVER)s" method="post" name="%(module)s" enctype="multipart/form-data">
		<input type="hidden" name="module" value="%(module)s">
		<input type="hidden" name="action" value="%(action)s">
		<input type="hidden" name="sid" value="%(sid)s">
		%(search_form)s
		%(result_msg)s
		<div id="main_body">
			%(main_body)s
		</div>
		</form>
		</body>
		</html>
		""" % pard
	return html


def render_menu(pard):
	pard['current_connection'] = '%(USER)s@%(HOST)s/%(DB)s' % pard
	pard['TITLE'] = '%(TITLE)s - %(current_connection)s' % pard
	h = """
	<table width="100%%" class="header" style="background-color: %(COLOR)s;">
		<tr>
			<td class="left">MySQL Web</td>
			<td class="right">%(USER)s@%(HOST)s/<span style="font-size: 2em;">%(DB)s</span></td>
		</tr>
	</table>
	""" % pard
	return h


def render_edit_field(pard):
	pard['field_value'] = str(pard['field_value'])
	pard['textarea_rows'] = pard['field_value'].split('\n')
	pard['t_rows'] = len(pard['textarea_rows'])
	pard['text_size'] = 3
	for item in pard['textarea_rows']:
		if len(item) > pard['text_size']:
			pard['text_size'] = len(item)
	if pard['text_size'] > 80:
		rows = pard['text_size'] / 80 + 1
		if rows > pard['t_rows']:
			pard['t_rows'] = rows
		pard['text_size'] = '80'
	else:
		pard['text_size'] = str(pard['text_size'])
	
# 	pk_dict = cjson.decode(pard['pk'])
# 	pard['pk_json'] = cjson.encode(pk_dict) 
	
	pard_js =  '{module: "query",'
	pard_js += 'appserver: "%(APPSERVER)s",'
	pard_js += 'sid: "%(sid)s",'
	pard_js += 'table_name: "%(table_name)s",'
	pard_js += 'field_name: "%(field_name)s",'
	pard_js += 'pk: %(pk)s,'
	pard_js += 'area: "%(area)s",'
	pard_js = pard_js % pard
	
	pard['pard_js_update'] = pard_js + 'action: "update_field"}'
	pard['pard_js_restore'] = pard_js +  'action: "restore_field"}'
	
	if pard['t_rows'] > 1 or pard['field_value'].find('"') > -1:
		h = """
			<textarea id="field_%(area)s" name="%(field_name)s" cols="%(text_size)s" rows="%(t_rows)s">
			%(field_value)s
			</textarea>
			<img src="/img/save.png" title="Save" onclick='update_field(%(pard_js_update)s);'>&nbsp;
			<img src="/img/uncheck.png" title="Cancel" onclick='restore_field(%(pard_js_restore)s);'>
			""" % pard
	else:
		h = """
			<input id="field_%(area)s" type="text" name="%(field_name)s" size="%(text_size)s" value="%(field_value)s">
			<img src="/img/save.png" title="Save" onclick='update_field(%(pard_js_update)s);'>&nbsp;
			<img src="/img/uncheck.png" title="Cancel" onclick='restore_field(%(pard_js_restore)s);'>
			""" % pard
	return h


def db_get_field_value(pard):
	pk_dict = cjson.decode(pard['pk'])
	pard['sql']	= "select %(field_name)s from %(table_name)s where " % pard
	l = []
	for key in pk_dict.keys():
		if isinstance(pk_dict[key], str):
			l.append("%s = '%s'" % (key, pk_dict[key].replace("'", "''")))
		else:
			l.append("%s = %s" % (key, str(pk_dict[key])))
	pard['sql'] += ' and '.join(l)
	result = db.db_access.send(pard)
	return str(result[0][0])


def db_update_field(pard):
	pk_dict = cjson.decode(pard['pk'])
	pard['field_value'] = pard.get('field_value', '')
	pard['field_value'] = pard['field_value'].replace("'", "''")
	pard['sql']	= "update %(table_name)s set %(field_name)s = '%(field_value)s' where " % pard
	l = []
	for key in pk_dict.keys():
		if isinstance(pk_dict[key], str):
			l.append("%s = '%s'" % (key, pk_dict[key].replace("'", "''")))
		else:
			l.append("%s = %s" % (key, str(pk_dict[key])))
	pard['sql'] += ' and '.join(l)
	dummy = db.db_access.send_dict(pard)
	return pard['sql']


def delete_row(pard):
	pk_dict = cjson.decode(pard['pk_del'])
	pard['sql'] = "delete from %(tbl_del)s where " % pard
	l = []
	for key in pk_dict.keys():
		if isinstance(pk_dict[key], str):
			l.append("%s = '%s'" % (key, pk_dict[key].replace("'", "''")))
		else:
			l.append("%s = %s" % (key, str(pk_dict[key])))
	pard['sql'] += ' and '.join(l)
	result = db.db_access.send_dict(pard)
	result['sql'] = pard['sql']
	query_result = '"%(sql)s" Affected row: <b>%(rowcount)s</b> (<b>%(exec_time)s</b> seconds)' % result
	return query_result


def prepare_insert(pard):
	pk_dict = cjson.decode(pard['pk'])
	pard['sql'] = "select * from %(table_name)s where " % pard
	l = []
	for key in pk_dict.keys():
		if isinstance(pk_dict[key], str):
			l.append("%s = '%s'" % (key, pk_dict[key].replace("'", "''")))
		else:
			l.append("%s = %s" % (key, str(pk_dict[key])))
	pard['sql'] += ' and '.join(l)
	result = db.db_access.send_dict(pard)
	if result['rowcount'] == 0:
		return 'Not Found'
	rec = result['rows'][0]
	columns_list = []
	value_list = []
	for c_name in result['c_name']:
		columns_list.append(c_name)
		if isinstance(rec[c_name], str):
			value_list.append("'%s'" % rec[c_name].replace("''", "'"))
		else:
			value_list.append("%s" % rec[c_name])
	pard['sql'] = 'INSERT INTO %s\n(%s)\nVALUES\n(%s);' % (
		pard['table_name'],
		', '.join(columns_list),
		', '.join(value_list),
		)
	return pard['sql']
	
	


def get_query_history(pard):
	history_file = pard['DATA_DIR'] + '/%(REMOTE_IP_ADDR)s/%(USER)s_%(HOST)s_%(DB)s.txt' % pard
	try:
		fp = open(history_file, 'r')
		buf = fp.read()
		fp.close()
	except IOError:
		buf = ''
	query_list = buf.split('<end>')
	hist = []
	for q in query_list:
		if not q:
			continue
		rec = {}
		try:
			rec['datetime'], rec['query'] = q.split(':::')
			hist.append(rec)
		except:
			pass
	return hist


def set_query_history(pard):
	history_file = pard['DATA_DIR'] + '/%(REMOTE_IP_ADDR)s/%(USER)s_%(HOST)s_%(DB)s.txt' % pard
	pard['now'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
	fp = open(history_file, 'a')
	fp.write('%(now)s:::%(query)s<end>' % pard)
	fp.close()
	

def js_navhistory(pard):
	pard['query_history'] = get_query_history(pard)[-50:]
	qh = []
	for rec in pard['query_history']:
		qh.append("'" + rec['query'].replace('\\', '\\\\').replace("'", "\\'").replace('\n', '\\n').replace('\r', '\\r').replace('"', '\"') + "'")
	qh.append("''")
	pard['qh'] = ', '.join(qh)
	js = """
		<script type="text/javascript" language="javascript" charset="utf-8">
		function navhistory(direction) {
			qh = [%(qh)s];
			i = document.getElementById('query_index').value
			
			if (direction == 'next') {
				i++
				};
			if (direction == 'prev') {
				i--
				};
			if (i < 0) {
				i = 0
				};
			if (i > qh.length - 1) {
				i = qh.length - 1
				};
			
			document.getElementById('query').value = qh[i];
			document.getElementById('query_index').value = i;			
			}
		</script>
		""" % pard
	return js


def render_query_history(pard):
	pard['history'].reverse()
	h = ["""
		<script type="text/javascript" language="javascript" charset="utf-8">
		function set_query(query) {
			document.getElementById('query').value = query;
			document.getElementById('query').focus();
		}
		</script>
		<table width=100%>
		<tr>
			<th scope="col">Query</th>
			<th scope="col">Date and Time</th>
		</tr>
		"""]
	for rec in pard['history']:
		rec['query_esc'] = tools.escape_javascript(rec['query'])
		h.append("""
			<tr>
				<td title="Set query" onClick="set_query('%(query_esc)s')"><pre>%(query)s</pre></td>
				<td>%(datetime)s</td>
			</tr>
			""" % rec)
	h.append('</table>')
	return '\n'.join(h)


def wsel_query_wizard(pard):
	args = pard['sql_query'][4:].replace(',', ' ').split()
	tables_list = db.db_access.get_db_tables_list(pard)
	error = ''
	if len(args) == 0:
		error = 'Syntax: wsel table_name [, table_name]'
	elif len(args) == 1:
		join = False
	else:
		join = True
		alias = 'abcdefghijklmn'
	i = 0
	columns = []
	tables = []
	for pard['table_name'] in args:
		if pard['table_name'] not in tables_list:
			error = 'Table %(table_name)s doesn\'t exist' % pard
			break
		columns_list = db.db_access.show_columns(pard, out='list')
		if join:
			columns_list = ['%s.%s' % (alias[i], x) for x in columns_list]
			pard['table_name'] = '%s %s' % (pard['table_name'], alias[i])
		columns.append(', '.join(columns_list))
		tables.append(pard['table_name'])
		i +=1
	if error:
		pard['msg'] = '<span class="err">%s</span>' % error
		pard['result_msg'] = render_result_msg(pard)
	else:
		pard['query'] = 'SELECT ' + ',\n'.join(columns) + '\nFROM ' + ', '.join(tables)
	pard['search_form'] = render_search_form(pard)
	return pard


def wins_query_wizard(pard):
	args = pard['sql_query'][4:].replace(',', ' ').split()
	tables_list = db.db_access.get_db_tables_list(pard)
	error = ''
	if len(args) != 1:
		error = 'Syntax: wins table_name'
	else:
		pard['table_name'] = args[0]
		if pard['table_name'] not in tables_list:
			error = 'Table %(table_name)s doesn\'t exist' % pard
	if error:
		pard['msg'] = '<span class="err">%s</span>' % error
		pard['result_msg'] = render_result_msg(pard)
	else:
		columns_list = db.db_access.show_columns(pard, out='list')
		value_list = ["'%%(%s)s'" % x for x in columns_list]
		pard['query'] = 'INSERT INTO %s\n(%s)\nVALUES\n(%s)' % (
			pard['table_name'],
			', '.join(columns_list),
			', '.join(value_list),
			)
	pard['search_form'] = render_search_form(pard)
	
	return pard



def silk_icons(pard):
	args = pard['sql_query'][4:].replace(',', ' ').split()
	if args:
		pard['icon_name'] = args[0]
	else:
		pard['icon_name'] = ''
	pard['icon_name'] = pard['icon_name'].lower()
	
	lf = os.listdir('%(HTML_DIR)s/icons/' % pard)
	i = 0
	pard['cols'] = 10
	h = ['<table width="100%%">']
	for ic in lf:
		if ic[-4:] <> '.png':
			continue
		if pard['icon_name']:
			if ic.lower().find(pard['icon_name']) == -1:
				continue
		if i % pard['cols'] == 0:
			h.append('<tr>')
		pard['icon_file_name'] = ic
		h.append("""
			<td title="%(icon_file_name)s">
				<img src="/icons/%(icon_file_name)s" alt="%(icon_file_name)s" border="0" onClick="$('%(icon_file_name)s').toggle();">
				<span class="small" style="display: none;" id="%(icon_file_name)s">%(icon_file_name)s</span>
			</td>
			""" % pard)
		i +=1
		if i % pard['cols'] == 0:
			h.append('</tr>')
	
	if i % pard['cols'] != 0:
		h.append('</tr>')
	
	h.append('</table>')
	pard['main_body'] = '\n'.join(h)
	pard['result_msg'] = render_result_msg(pard)
	pard['search_form'] = render_search_form(pard)
	
	return pard


# def eval_query(pard):
# 	"""Troppo pericoloso Si possono fare os.system() ..."""
# 	code = pard['query'][5:] #.replace('\r\n', '\n')
# 	try:
# 		result = eval(code)
# 	except:
# 		result = tools.build_exception()
# 		result += '<pre>%s<pre>' % repr(code)
# 	pard['main_body'] = result
# 	pard['result_msg'] = render_result_msg(pard)
# 	pard['search_form'] = render_search_form(pard)
# 	
# 	return pard


def ajax_allow_edit_field(pard):
	html = """
		<script type="text/javascript" language="javascript" charset="utf-8">
	
		function allow_edit_field(pard_js)
		{
			//area: name of the target box
			//field_name
			//pk: json object of row primary key
			params = "module=" + escape("%(module)s");
			params += "&sid=" + escape("%(sid)s");
			params += "&table_name=" + escape("%(table_name)s");
			params += "&action=allow_edit_field";
			params += "&area=" + escape(pard_js.area);
			params += "&field_name=" + escape(pard_js.field_name);
			params += "&pk=" + escape(Object.toJSON(pard_js.pk));
			
			// To disable DblClick in field edit mode.
			var edit_mode = $('field_' + pard_js.area);
			if (edit_mode != null) {
				return true
				}
			
			req = new Ajax.Request
			(
				"%(APPSERVER)s",
				{
					method: 'post',
					parameters: params,
					onLoading: function()
					{
						//document.getElementById(pard_js.area).innerHTML = '<img src="/img/progress.gif">';
						$(pard_js.area).innerHTML = '<img src="/img/progress.gif">';
					},
					onComplete: function(resp)
					{
						//document.getElementById(pard_js.area).innerHTML = resp.responseText;
						$(pard_js.area).innerHTML = resp.responseText;
					}
				}
			)
		}


		function row_delete(pard_js)
		{
			if (confirm("Confirm delete?"))
			{
				document.forms['query'].elements['action'].value = 'row_delete';
				$('tbl_del').value = pard_js.table_name;
				$('pk_del').value = Object.toJSON(pard_js.pk);
				document.forms['query'].submit();	
			}
		}


		function prepare_insert(pard_js)
		{
			//table_name
			//pk: json object of row primary key
			params = "module=" + escape("%(module)s");
			params += "&sid=" + escape("%(sid)s");
			params += "&table_name=" + escape(pard_js.table_name);
			params += "&action=prepare_insert";
			params += "&pk=" + escape(Object.toJSON(pard_js.pk));
			if ($('query2_display').value == 'N')
			{
				toggle_multi_area();
			}
			req = new Ajax.Request
			(
				"%(APPSERVER)s",
				{
					method: 'post',
					parameters: params,
					onComplete: function(resp)
					{
						$('query2').value = $('query2').value + '\\n' + resp.responseText;
					}
				}
			)
		}
		
		</script>
		""" % pard
	return html


def ajax_js(pard):
	html = """
		<script type="text/javascript" language="javascript" charset="utf-8">

		function close_window(window)
		{
			document.getElementById(window).innerHTML = '';
		}

		function restore_field(pard_js)
		{
			//area: name of the target box
			//table_name
			//field_name
			//pk: json object of row primary key
			params = "module=" + escape(pard_js.module);
			params += "&action=" + escape(pard_js.action);
			params += "&area=" + escape(pard_js.area);
			params += "&sid=" + escape(pard_js.sid);
			params += "&field_name=" + escape(pard_js.field_name);
			params += "&table_name=" + escape(pard_js.table_name);
			params += "&pk=" + escape(Object.toJSON(pard_js.pk));
			req = new Ajax.Request
			(
				pard_js.appserver,
				{
					method: 'post',
					parameters: params,
					onLoading: function()
					{
						document.getElementById(pard_js.area).innerHTML = '<img src="/img/progress.gif">';
					},
					onComplete: function(resp)
					{
						document.getElementById(pard_js.area).innerHTML = resp.responseText;
					}
				}
			)
		}
		
		function update_field(pard_js)
		{
			//area: name of the target box
			//table_name
			//field_name
			//pk: json object of row primary key
			params = "module=" + escape(pard_js.module);
			params += "&action=" + escape(pard_js.action);
			params += "&area=" + escape(pard_js.area);
			params += "&sid=" + escape(pard_js.sid);
			params += "&field_name=" + escape(pard_js.field_name);
			params += "&table_name=" + escape(pard_js.table_name);
			params += "&pk=" + escape(Object.toJSON(pard_js.pk));
			params += "&field_value=" + escape(document.getElementById("field_" + pard_js.area).value);
			req = new Ajax.Request
			(
				pard_js.appserver,
				{
					method: 'post',
					parameters: params,
					onLoading: function()
					{
						document.getElementById(pard_js.area).innerHTML = '<img src="/img/progress.gif">';
					},
					onComplete: function(resp)
					{
						document.getElementById(pard_js.area).innerHTML = resp.responseText;
					}
				}
			)
		}
		
		function getSelection(text_el)
		{
			if(!!document.selection)
			  return document.selection.createRange().text;
			else if(text_el.setSelectionRange)
			  return text_el.value.substring(text_el.selectionStart,text_el.selectionEnd);
			else
			  return false;
		  }
		
		</script>
		""" % pard

	return html


if __name__ == '__main__':
	import config
	from pprint import pprint
	pard = config.global_parameters({})
	pard['HOST'] = '192.168.111.117'
	pard['USER'] = 'devel'
	pard['DB'] = 'devel'
	#tools.dump(get_query_history(pard))
	#print js_navhistory(pard)
	