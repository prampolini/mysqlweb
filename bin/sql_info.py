import re
#import db_access
import db

# ---------------------------------------------------------------------------
# Pattern definition
# ---------------------------------------------------------------------------
nn_line_pat = re.compile(r'\s*(#|$)')
first_word_pat = re.compile(r'\s*(\w+)')
doublequoted_text_pat = re.compile(r'("[^"\\]*(\\.[^"\\]*)*")', re.DOTALL | re.IGNORECASE)
quoted_text_pat = re.compile(r"('[^'\\]*(\\.[^'\\]*)*')", re.DOTALL | re.IGNORECASE)
words_pat = re.compile(r'(\w+)', re.DOTALL | re.IGNORECASE)
from_clause_pat = re.compile(r'\bfrom\b(.+?)(?=\bwhere\b|\blimit\b|group by|order by|\bfor\b|\Z)', re.DOTALL | re.IGNORECASE)
upd_from_clause_pat = re.compile(r'\update\b(.+?)(?=\bset\b|\Z)', re.DOTALL | re.IGNORECASE)
ins_from_clause_pat = re.compile(r'\into\b(.+?)(?=\bvalues\b|\(|\Z)', re.DOTALL | re.IGNORECASE)
wsel_from_clause_pat = re.compile(r' *wsel\b *\b(.*)\b', re.DOTALL | re.IGNORECASE)
# ---------------------------------------------------------------------------


def get_query_command(sql):
	sql_lines = sql.split('\n')
	command = ''
	for line in sql_lines:
		# discard blank and comment lines
		if nn_line_pat.match(line):
			pass
		else:
			m = first_word_pat.match(line)
			if m:
				command = m.group(1).lower()
			break
	return command


def get_sql_info(pard, flag_column_info=True):
	"""Get information about pard['sql_query']"""
	
	sql_info = {
		'query_command': '',
		'words': [],
		'select_tables_list': [],
		'primary_key': [], # if only one table was selected
		'table_columns': [], # "
		}
	
	sql_info['query_command'] = get_query_command(pard['sql_query'])
	sql_temp = doublequoted_text_pat.sub('', pard['sql_query'])
	sql_temp = quoted_text_pat.sub('', sql_temp)
	sql_info['words'] = words_pat.findall(sql_temp)
	sql_info['words'] = [x.lower() for x in sql_info['words']]
	if sql_info['query_command'] == 'select':
		# Get tables lists in the select statement
		# using sql_temp 
		# (sql command whithout quoted and double quoted text)
		from_clause = from_clause_pat.findall(sql_temp)
		if from_clause:
			from_clause = from_clause[0]
			tables_list = db.db_access.get_db_tables_list(pard)
			tables_pat = re.compile(r'(\b(?:%s)\b)' % '|'.join(tables_list), re.DOTALL | re.IGNORECASE)
			sql_info['select_tables_list'] = tables_pat.findall(from_clause)
			if len(sql_info['select_tables_list']) == 1 and flag_column_info:
				pard['db_table'] = sql_info['select_tables_list'][0]
				sql_info['primary_key'] = db.db_access.get_primary_key(pard)
				sql_info['table_columns'] = db.db_access.show_columns(pard, out='list')
	
	# [Bettelli's improvement 11-12-09]
	elif sql_info['query_command'] == 'update':
		from_clause = upd_from_clause_pat.findall(sql_temp)
		if from_clause:
			from_clause = from_clause[0]
			tables_list = db.db_access.get_db_tables_list(pard)
			tables_pat = re.compile(r'(\b(?:%s)\b)' % '|'.join(tables_list), re.DOTALL | re.IGNORECASE)
			sql_info['select_tables_list'] = tables_pat.findall(from_clause)
	elif sql_info['query_command'] == 'insert':
		from_clause = ins_from_clause_pat.findall(sql_temp)
		if from_clause:
			from_clause = from_clause[0]
			tables_list = db.db_access.get_db_tables_list(pard)
			tables_pat = re.compile(r'(\b(?:%s)\b)' % '|'.join(tables_list), re.DOTALL | re.IGNORECASE)
			sql_info['select_tables_list'] = tables_pat.findall(from_clause)
	elif sql_info['query_command'] in ('wsel', 'wins'):
		from_clause = wsel_from_clause_pat.findall(sql_temp)
		if from_clause:
			from_clause = from_clause[0]
			tables_list = db.db_access.get_db_tables_list(pard)
			tables_pat = re.compile(r'(\b(?:%s)\b)' % '|'.join(tables_list), re.DOTALL | re.IGNORECASE)
			sql_info['select_tables_list'] = tables_pat.findall(from_clause)
	# [end of Bettelli's improvement 11-12-09]
	
	return sql_info


if __name__ == '__main__':
	import config
	from pprint import pprint
	pard = config.global_parameters({})
	pard['HOST'] = '192.168.111.117'
	pard['USER'] = 'devel'
	pard['DB'] = 'devel'
	pard['PASSWORD'] = '******'
	#tools.dump(db.db_access.get_db_tables_list(pard))
	
	fp = open('prova_get_sql_info.sql', 'r')
	pard['sql_query'] = fp.read()
	fp.close()
	print pard['sql_query']
	pprint(get_sql_info(pard))
	print '*'*70
	import query
	h = query.get_query_history(pard)
	for rec in h[-5:]:
		pard['sql_query'] = rec['query']
		print rec['query']
		pprint(get_sql_info(pard))
		r = raw_input('continue...')
		if r.lower() == 'q':
			break
		#print '\n'*2