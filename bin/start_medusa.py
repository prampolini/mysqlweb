#! /usr/local/bin/python

# Per lanciare in background:
# python start_medusa.py > ../log/medusa.log 2>&1 &
# python /home/gm/mysqlweb/bin/start_medusa.py > /home/gm/mysqlweb/log/medusa.log 2>&1 &

import sys
import os
import pwd
import asyncore

from medusa import http_server
from medusa import monitor
from medusa import filesys
from medusa import default_handler
from medusa import script_handler
from medusa import status_handler
from medusa import virtual_handler
from medusa import resolver
from medusa import logger
from medusa import put_handler

import config
import mysqlweb

def start_medusa(pard):
	dns_resolver = None
	#log = logger.file_logger(pard['COMMON_LOG_FILE'])
	log = logger.file_logger(sys.stdout)
	log = status_handler.logger_for_status(log)
	filesystem = filesys.os_filesystem(pard['HTML_DIR'])
	default_http_handler = default_handler.default_handler(filesystem)
	web_server = http_server.http_server(pard['IP_ADDRESS'], pard['HTTP_PORT'], dns_resolver, log)
	#virtual_http_handler = virtual_handler.virtual_handler_with_host(default_http_handler, pard['DNS'])
	#web_server.install_handler(virtual_http_handler)
	web_server.install_handler(default_http_handler)
	#publishing_filesystem = filesys.os_filesystem(pard['HTML_DIR'])
	#publishing_http_handler = put_handler.put_handler(publishing_filesystem, '/.*')
	#web_server.install_handler(publishing_http_handler)
	script = script_handler.persistent_script_handler()
	script.add_module('', mysqlweb)
	web_server.install_handler(script)
	monitor_server = monitor.secure_monitor_server('pippone', '', pard['MONITOR_PORT'])
	status_objects = [web_server, monitor_server, dns_resolver, log]
	status = status_handler.status_extension(status_objects)
	web_server.install_handler(status)
	if os.name == 'posix':
		if hasattr(os, 'setuid'):
			[uid, gid] = pwd.getpwnam('gian')[2:4]
			os.setuid(uid)
			#os.setgid(gid)
	asyncore.loop()




if __name__ == '__main__':
	pard = config.global_parameters(pard={})
	start_medusa(pard)
