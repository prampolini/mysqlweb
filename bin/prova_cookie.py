import Cookie
import time


def js_create_cookie(pard):
	c = Cookie.SimpleCookie()
	c['cookie_sid'] = pard['sid']
	c['cookie_sid']['path'] = '/'
	tomorrow = time.time() + 86400
	expGMT = time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime(tomorrow))
	c['cookie_sid']['expires'] = expGMT
	return c.js_output()


def get_cookie(pard):
	h = []
	if 'HTTP_COOKIE' in pard:
		c = Cookie.SimpleCookie(pard['HTTP_COOKIE'])
		for m in c:
			key = c[m].key
			value = c[m].value
			pard[key] = value
			h.append('%s: %s' % (key, value))
	else:
		h.append('Non ci sono Cookie')
	return '\n'.join(h)


if __name__ == '__main__':
	pard = {}
	
	print '--- test js_create_cookie ---'
	pard['sid'] = '11223344556677889900'
	print js_create_cookie(pard)
	
	print
	print
	print '--- test get_cookie ---'
	pard = {}
	print 'pard prima: %s' % pard
	pard['HTTP_COOKIE'] = 'cookie_sid=11223344556677889900; expires=Tue, 23 Oct 2007 11:43:10 GMT'
	print get_cookie(pard)
	print
	print 'pard dopo: %s' % pard