#! /usr/local/bin/python -u

#
# Driver SCGI
#
# Author: Marco Tasselli (scgi engine)
#         Gianfranco Messori (application server)
#         Claudio Cilloni (process handling)
#
# Date:  December 2004 (first release) - May 2007 (last revision)
#

import os
import sys
import signal
import traceback
import string
import time
import cgi
import config
import auth
import tools
import socket

from scgi import scgi_server

pid_file = '/home/gm/mysqlweb/mysqlweb.pid'

class Driver_handler(scgi_server.SCGIHandler):

	def __init__(self, parent_fd):
		scgi_server.SCGIHandler.__init__(self, parent_fd)


	def handle_connection(self, conn):
		start = time.time()
		pard = {}
		pard['ERROR'] = ''
		pard['html'] = ''
		pard['file_upload'] = []
		html = ''
		error_log = ''
		error_sid = ''
		command = ''
		id_utente = ''
		pard['header'] = 'Content-type: text/html\n\n'
		try:
			input = conn.makefile("r")
			output = conn.makefile("w")
			pard.update(self.read_env(input))
			form = cgi.FieldStorage(fp=input, headers=None, outerboundary="", environ=pard, keep_blank_values=1, strict_parsing=0)
			for k in form.keys():
				if k not in pard: # disable environment key overwrite
					if (k[0:12] == 'file_upload_') and (form[k].filename != ''):
						pard['file_upload'].append({'filename': form[k].filename, 'content': form.getvalue(k, ''), 'form_id': k})
					else:
						pard[k] = form.getvalue(k, '')
			pard['HTTP_REFERER'] = pard.get('HTTP_REFERER', '')
			pard['REMOTE_ADDR'] = pard.get('REMOTE_ADDR', '')
			pard['HTTP_X_FORWARDED_FOR'] = pard.get('HTTP_X_FORWARDED_FOR', '')
			if pard['HTTP_X_FORWARDED_FOR']:
				pard['REMOTE_IP_ADDR'] = pard['HTTP_X_FORWARDED_FOR']
			else:
				pard['REMOTE_IP_ADDR'] = pard['REMOTE_ADDR']
			pard = config.global_parameters(pard)
			pard = auth.check_sid(pard)
			if pard['flag_sid'] == 'NEW':
				pard['tmp_sid'] = auth.calc_sid(pard)
				pard['module'] = 'auth'
				pard['program'] = 'login'
			
			if pard['flag_sid'] == 'KO':
				flag = 'stop'
			else:
				pard['program'] = pard.get('program', 'main')
				command = pard['module'] + '.' + pard['program'] + '(pard)'
				if command in ['auth.login(pard)', 'auth.check_authentication(pard)']:
					flag = 'LOGIN'
				else:
					pard = auth.load_sid_data(pard)
					if pard['module'] in pard['AUTH_MODULE']:
						flag = 'GO'
					else:
						flag = 'STOP'
			
			if flag in ('GO', 'LOGIN'):
				if pard['DEBUG'] == 1:
					reload_developing_modules(pard)
				if __import__(pard['module']).__dict__.has_key(pard['program']):
					pard = __import__(pard['module']).__dict__[pard['program']](pard)
			else:
				pard = auth.build_not_authorized(pard)
			
			stop = time.time()
			pard['sid_user'] = pard.get('sid_user', '')
			error_log = log_hit(start, stop, pard['REMOTE_ADDR'], pard['sid_user'], command, len(pard['html']), pard['ERROR'], pard)
			error_sid = log_sid(start, stop, len(html), pard)
			output.write(pard['header'])
			if pard['header'] == 'Content-type: text/html\n\n':
				pard['html'] = pard['html'].replace('\t', '')
			output.write(pard['html'])
			if error_log:
				output.write(error_log)
			if error_sid:
				output.write(error_sid)
		except:
			stop = time.time()
			pard['ERROR'] = build_exception()
			try:
				pard = auth.error_manager(pard)
				pard['html'] = string.replace(pard['html'], '\t', '')
				output.write(pard['header'])
				output.write(pard['html'])
			except:
				output.write(pard['header'])
				output.write(pard['ERROR'])
			error_log = log_hit(start, stop, pard['REMOTE_ADDR'], id_utente, command, len(html), pard['ERROR'], pard)
			error_sid = log_sid(start, stop, len(html), pard)
			if error_log:
				output.write(error_log)
			if error_sid:
				output.write(error_sid)
		exec_time = repr(stop - start)[0:5]
		#output.write('<br><br><center><font size=1><exec_time>Exec Time: ' + exec_time + '</exec_time></font></center>')
		output.close()
		input.close()
		conn.close()


def build_exception(type=None, value=None, tb=None, limit=None):
	if type is None:
		type, value, tb = sys.exc_info()
	buf = "<H3>Traceback (most recent call last):</H3>"
	list = traceback.format_tb(tb, limit) + traceback.format_exception_only(type, value)
	buf = buf + "<PRE>%s<B>%s</B></PRE>" % (escape("".join(list[:-1])), escape(list[-1]))
	del tb
	return buf


def escape(s, quote=None):
	s = s.replace("&", "&amp;") # Must be done first!
	s = s.replace("<", "&lt;")
	s = s.replace(">", "&gt;")
	if quote:
		s = s.replace('"', "&quot;")
	return s


def log_hit(start, stop, ip, id_utente, command, bytes, error, pard):
	try:
		action = pard.get('action', '')
		id_utente = pard.get('sid_user', '')
		h = pard.get('html', '')
		bytes = len(h)
		exec_time = repr(stop - start)[0:5]
		log_file = open(pard['APPLICATION_LOG_FILE'], 'a')
		log_date = time.asctime(time.localtime(time.time()))
		error_id = ''
		if error != '':
			error_id = string.split(repr(stop), '.')[0]
			error = string.replace(error[0], '\n', '\040')
			error = 'ERROR_FILE -> ' + error_id + '.err'
		log_buf = string.join([log_date, id_utente, ip, repr(bytes), exec_time, command, action, pard['HTTP_REFERER'], error], '\t') + '\n'
		log_file.write(log_buf)
		log_file.close()
		if error_id != '':
			error_file = pard['LOG_DIR'] + '/' + error_id + '.err'
			fp = open(error_file, 'w')
			fp.write(error + '\n')
			keys = pard.keys()
			keys.sort()
			for k in keys:
				buf = repr(k) + ': ' + repr(pard[k]) + '\n'
				fp.write(buf)
			fp.close()
		#auth.log_hit(log_buf, pard)
		return ''
	except:
		return build_exception()


def log_sid(start, stop, bytes, pard):
	if pard.has_key('sid'):
		try:
			exec_time = repr(stop - start)[0:5]
			sid_file = auth.sid_get(pard)
			log_date = time.asctime(time.localtime(time.time()))
			pard['EXEC_TIME'] = exec_time
			pard['LOG_DATE'] = log_date
			pard['BYTES'] = repr(bytes)
			sid_file['sid_buffer'] = pard.get('sid_buffer', '')
			result = auth.sid_set(pard, sid_file)
			return ''
		except:
			return build_exception()


def reload_developing_modules(pard):
	try:
		buf = open('%(BIN_DIR)s/developing_modules' % pard, 'r').read()
	except IOError:
		return []
	modules_list = buf.split()
	for item in modules_list:
		cmd = 'import %s;reload(%s)' % (item, item) 
		exec(cmd)
	return modules_list


def session_detach():

	if os.getppid() == 1:
		# il padre e' init, non c'e' bisogno di staccarsi
		pass

	else:

		## ignora i segnali provenienti dal terminale
		signal.signal(signal.SIGTTOU, signal.SIG_IGN)
		signal.signal(signal.SIGTTIN, signal.SIG_IGN)
		signal.signal(signal.SIGTSTP, signal.SIG_IGN)

		## lascia che il processo chiamante continui la sua esecuzione
		if os.fork() != 0:
			sys.exit(0)

		## lascia il terminale e cambia gruppo
		os.setpgrp()

		## ignora la morte del processo leader del gruppo
		signal.signal(signal.SIGHUP, signal.SIG_IGN)

		## diventa non-leader del gruppo
		if os.fork() != 0:
			sys.exit(0)

	## chiudi i file descriptor per stdin, stdout, stderr
	os.close(sys.stdin.fileno())
	os.close(sys.stdout.fileno())
	os.close(sys.stderr.fileno())

	## sposta la directory corrente su un filesystem che
	## non verra' sicuramente smontato
	os.chdir('/')

	## annulla qualunque modo di creazione file ereditato
	os.umask(0)

	# fine.


def kill_callback(sig, stack_frame):
	'''
	gestione del kill
	'''
	os.remove(pid_file)
	now = tools.get_time()
	print '%s - scgi server stopped' % now
	sys.exit(0)


def start(pard):
	if os.path.isfile(pid_file):
		print 'sdriver already running - PID FILE: %s' % pid_file
		sys.exit(1)
	
	## Test Address
	for i in range(10):
		try:
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			s.connect(('localhost', 4090))
			s.close()
			print '.',
			time.sleep(1)
		except socket.error:
			break
	else:
		print 'socket.error: Address already in use'
		sys.exit(1)
	
	print 'Starting scgi server ...'
	session_detach()
	fp = open(pid_file, 'w')
	fp.write(str(os.getpid()))
	fp.close()
	
	fstdout = file(pard['SCGI_LOG_FILE'], 'a', 0)
	sys.stdout = fstdout
	sys.stderr = fstdout
	
	### gestione di un eventuale kill
	signal.signal(signal.SIGTERM, kill_callback)
	
	def create_handler(parent_fd):
		return Driver_handler(parent_fd)
	s = scgi_server.SCGIServer(create_handler, host='localhost', port=4090, max_children=20)
	s.serve()


def stop(pard):
	if os.path.isfile(pid_file):
		fp = open(pid_file, 'r')
		pid = fp.read()
		fp.close()
		os.kill(int(pid), signal.SIGTERM)
		time.sleep(1)


if __name__ == '__main__':
	pard = config.global_parameters({})
	if len(sys.argv) == 1:
		arg = 'start'
	else:
		arg = sys.argv[1]
	
	if arg == 'start':
		start(pard)
	elif arg == 'stop':
		stop(pard)
	elif arg == 'restart':
		stop(pard)
		start(pard)
	else:
		print 'Invalid arguments'
		sys.exit(2)
	
