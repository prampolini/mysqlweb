import os

import json

import tools


def main(pard):
	pard['module'] = 'preferences'
	pard['program'] = 'main'
	pard['action'] = pard.get('action', 'start')
	pard['menu'] = render_menu(pard)
	pard['search_form'] = ''
	pard['msg'] = []
	pard['result_msg'] = ''
	pard['main_body'] = ''
	pard['javascript'] = '<script src="/javascripts/prototype.js" type="text/javascript"></script>\n'
	pard['javascript'] += javascript(pard)
	
	# --------------------------- start ----------------------------
	if pard['action'] == 'start':
		pard['result'] = load_preferences(pard)
		pard['main_body'] = render_pref_form(pard)
	
	# --------------------------- save -----------------------------
	elif pard['action'] == 'save':
		pard['result'] = cgi_load_preferences(pard)
		result = save_preferences(pard)
		pard['save_message'] = result
# 		if result:
# 			pard['save_message'] = 'Saved!'
# 		else:
# 			pard['save_message'] = 'Error when saving your preferences!'
		pard['result'] = load_preferences(pard)
		pard['main_body'] = render_pref_form(pard)
		
	# --------------------------- select_color ---------------------
	elif pard['action'] in ('select_color', 'select_color_redraw'):
		pard['hex_color_list'] = get_hex_color_list(pard)
		pard['html'] = render_color(pard)
		return pard
	
	
	else:
		pard['result_msg'] = tools.format_messaggio('Function not available. ("%(action)s")' % pard)
		pard['main_body'] = '<input type="button" name="submit_form" value="Back" onclick="javascript:history.go(-1);">'
	
	pard['html'] = render_page(pard)
	return pard


def render_pref_form(pard):
	if 'STORE_PASSWORD' in pard and pard['STORE_PASSWORD']:
		pard['render_password_label'] = '<th>Password</th>'
	else:
		pard['render_password_label'] = ''
	html = ["""
		<table class="excel">
			<tr>
				<th>&nbsp;</th>
				<th>Alias</th>
				<th>Ind</th>
				<th>User</th>
				<th>Host</th>
				<th>DB</th>
				%(render_password_label)s
				<th>Color</th>
				<th>Link</th>
			</tr>
		""" % pard]
	ind = 0
	pard['result'].append(prefs_empty_row(pard))
	table_array = {}
	for rec in pard['result']:	
		rec['row_index'] = str(ind)
		rec['row_id'] = 'row_%(row_index)s' % rec
		table_array[rec['row_id']] = ind
		rec['APPSERVER'] = pard['APPSERVER']
		if rec['alias'] != '':
			rec['link'] = '<a href="%(APPSERVER)s%(alias)s">%(alias)s</a>' % rec
		else:
			rec['link'] = ''
		if 'STORE_PASSWORD' in pard and pard['STORE_PASSWORD']:
			rec['render_password_input'] = '<td><input type="password" name="passw_%(row_index)s" id="passw_%(row_index)s" value="%(passw)s" size="12"></td>' % rec
		else:
			rec['render_password_input'] = ''
		html.append("""
			<tr id="%(row_id)s">
				<td nowrap="nowrap">
					<a href="javascript:delete_row('row_%(row_index)s')" title="Delete Row"><img src="/icons/delete.png" border="0"></a>
					<a href="javascript:move_down('row_%(row_index)s')" title="Move Down"><img src="/icons/arrow_down.png" border="0"></a>
					<a href="javascript:move_up('row_%(row_index)s')" title="Move Up"><img src="/icons/arrow_up.png" border="0"></a>
				</td>
				<td><input type="text" name="alias_%(row_index)s" id="alias_%(row_index)s" value="%(alias)s" size="20"></td>
				<td><input type="text" name="ind_%(row_index)s" id="ind_%(row_id)s" value="%(row_index)s" size="2"></td>
				<td><input type="text" name="user_%(row_index)s" id="user_%(row_index)s" value="%(user)s" size="12"></td>
				<td><input type="text" name="host_%(row_index)s" id="host_%(row_index)s" value="%(host)s" size="15"></td>
				<td><input type="text" name="db_%(row_index)s" id="db_%(row_index)s" value="%(db)s" size="12"></td>
				%(render_password_input)s
				<td nowrap="nowrap">
					<input type="text" name="color_%(row_index)s" id="color_%(row_index)s" value="%(color)s" size="7" onChange="$('sample_color_%(row_index)s').setStyle({ backgroundColor: this.value})" />
					<span id="sample_color_%(row_index)s"
					      style="background-color: %(color)s; cursor: pointer;"
					      onClick="select_color('%(row_index)s')"
					      title="Select Color"
					      >
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</span>
					<div id="area_%(row_index)s" class="html_title"></div>
				</td>
				<td style="	border: 1px solid #D6DDE6; padding: 4px;">%(link)s</td>
			</tr>
			""" % rec)
		ind += 1
	pard.setdefault('save_message', '')
	if pard['save_message']:
		pard['cancel_btn_value'] = 'Exit'
	else:
		pard['cancel_btn_value'] = 'Cancel'
	html.append("""
		<tr>
			<td colspan="7" style="text-align: right;">
				<span class="err">%(save_message)s</span>
				&nbsp;&nbsp;
				<input type="button" name="cancel_button" value="%(cancel_btn_value)s" class="btn" onClick='location="%(APPSERVER)s"' />
				&nbsp;&nbsp;
				<input type="button" name="save_button" value="Save" class="btn" 
				 onclick='action.value="save"; form.submit();' />
			</td>
		</tr>
		""" % pard)
	html.append('</table>')
	pard['preferences_rows'] = str(ind)
	pard['table_array'] = str(table_array)
	html.append('<input type="hidden" name="preferences_rows" id="preferences_rows" value="%(preferences_rows)s">' % pard)
	html.append("""
		<script type="text/javascript" language="javascript" charset="utf-8">
		table_array = %s
		</script>
		""" % pard['table_array'])
	html = '\n'.join(html)
	return html


def render_page(pard):
	html = """
		<html>
		<head>
		<meta charset="utf-8">
		%(CSS)s
		<title>%(TITLE)s</title>
		%(javascript)s
		<link title="MySQL Query Navigator" rel="search" type="application/opensearchdescription+xml" 
		href="%(APPSERVER)sOpenSearch.xml">
		
		<link rel="stylesheet" href="/ColorList.css" type="text/css">
		
		</head>
		<body>
		%(menu)s
		<form action="%(APPSERVER)s" method="post" name="%(module)s" enctype="multipart/form-data">
		<input type="hidden" name="module" value="%(module)s">
		<input type="hidden" name="action" value="%(action)s">
		<input type="hidden" name="sid" value="%(sid)s">
		%(search_form)s
		%(result_msg)s
		<div id="main_body">%(main_body)s</div>
		</form>
		</body>
		</html>
		""" % pard
	return html


def render_menu(pard):
	pard['current_connection'] = ''
	pard['TITLE'] = '%(TITLE)s' % pard
	h = """
	<table width="100%%" class="header">
		<tr>
			<td class="left">MySQL Web Connection Manager</td>
			<td class="right">&nbsp;</td>
		</tr>
	</table>
	""" % pard
	return h


def render_color(pard):
	h = ["""
		<table>
		<tr>
			<th>Name</th>
			<th>Hex</th>
			<th>&nbsp;</th>
		</tr>
		"""]
	ind = 0
	for item in pard['hex_color_list']:
		d = {
			'descr': item[1],
			'hex': item[0]}
		if ind % 2 == 0:
			d['bg_class'] = 'alternate'
		else:
			d['bg_class'] = 'normal'
		h.append("""
			<tr class="%(bg_class)s" onClick="set_color('%(hex)s');" title="Set Color">
				<td>%(descr)s</td>
				<td>%(hex)s</td>
				<td style="background-color: %(hex)s; width: 30px;"></td>
			</tr>
			""" % d)
		ind += 1
	h.append('</table>')
	pard['color_list_table'] = '\n'.join(h)
	
	if pard['action'] == 'select_color':
		pard['area'] = 'area_%(row_index)s' % pard
		html = """
			<div id="color_list">
			<input type="hidden" name="row_index" id="row_index" value="%(row_index)s" />
			<input type="text" name="color_list_search" id="color_list_search" value="%(color_list_search)s" 
				onKeyUp='redraw(this, event);'
				/>
			<img src="/img/uncheck.png" border="0" onClick="$('%(area)s').innerHTML = '';">
			<div id="color_list_table">%(color_list_table)s</div>
			</div>
			""" % pard
		return html
	else:
		return pard['color_list_table']


def cgi_load_preferences(pard):
	prefs_rows = int(pard['preferences_rows'])
	result = []
	for i in xrange(prefs_rows):
		rec = {}
		try:
			rec['alias'] = pard['alias_%s' % str(i)]
		except KeyError:
			continue
		try:
			rec['ind'] = int(pard['ind_%s' % str(i)])
		except ValueError:
			rec['ind'] = 999
		try:
			rec['passw'] = pard['passw_%s' % str(i)]
		except KeyError:
			rec['passw'] = ''
		
		rec['user'] = pard['user_%s' % str(i)]
		rec['host'] = pard['host_%s' % str(i)]
		rec['db'] = pard['db_%s' % str(i)]
		rec['color'] = pard['color_%s' % str(i)]
		result.append(rec)
	result = sort_by(result, 'ind')
	return result


def load_preferences(pard):
	try:
		buf = open('%(DATA_DIR)s/%(REMOTE_IP_ADDR)s/preferences.txt' % pard, 'r').read()
	except IOError:
		return []
	prefs = json.loads(buf)
	for rec in prefs:
		rec['alias'] = rec.get('alias', '')
		rec['user'] = rec.get('user', '')
		rec['host'] = rec.get('host', '')
		rec['db'] = rec.get('db', '')
		if 'STORE_PASSWORD' and pard['STORE_PASSWORD']:
			rec['passw'] = rec.get('passw', '')
		else:
			rec['passw'] = ''
		rec['color'] = rec.get('color', pard['DEFAULT_COLOR'])
	return prefs


def load_alias_pref(pard):
	"""Get dictionary preferences for pard['path_alias']"""
	prefs = load_preferences(pard)
	result = {}
	for rec in prefs:
		if rec['alias'] == pard['path_alias']:
			result.update(rec)
			break
	return result


def prefs_empty_row(pard):
	rec = {}
	rec['alias'] = ''
	rec['user'] = ''
	rec['host'] = ''
	rec['db'] = ''
	rec['passw'] = ''
	rec['color'] = pard['DEFAULT_COLOR']
	return rec


def save_preferences(pard):
	try:
		backup = open('%(DATA_DIR)s/%(REMOTE_IP_ADDR)s/preferences.txt' % pard, 'r').read()
		open('%(DATA_DIR)s/%(REMOTE_IP_ADDR)s/preferences.bck' % pard, 'w').write(backup)
	except IOError:
		backup = ''
	pard['prefs'] = []
	for rec in pard['result']:
		if (rec['alias'], rec['host'], rec['db'], rec['user']) == ('', '', '', ''):
			continue
		else:
			for k in ['alias', 'passw', 'user']:
				try:
					rec[k] = unicode(rec[k])
				except UnicodeDecodeError:
					return 'I don\'t like `%s` you entered!' % k
			pard['prefs'].append(rec)
	json_prefs = json.dumps(pard['prefs'])
	try:
		open('%(DATA_DIR)s/%(REMOTE_IP_ADDR)s/preferences.txt' % pard, 'w').write(json_prefs)
	except IOError:
		return 'Unable to open your preferences file.'
	return 'Saved!'


def add_row(pard, rec):
	prefs = load_preferences(pard)
	new_rec = prefs_empty_row(pard)
	new_rec.update(rec)
	prefs.append(new_rec)
	pard['result'] = prefs
	return save_preferences(pard)


def convert_connection_history(pard):
	ll = os.listdir(pard['DATA_DIR'])
	for item in ll:
		if  item[0:3].isdigit() \
		and os.path.isdir(pard['DATA_DIR'] + '/' + item):
			try:
				buf = open(pard['DATA_DIR'] + '/' + item + '/conn_history.txt').read()
				connection_history = buf.split('\n')[:-1]
				prefs = []
				ind  = 0
				for elem in connection_history:
					ind += 1
					d = {'ind': ind}
					d['user'], d['host'], d['db'] = elem.split('\t')
					prefs.append(d)
				json_prefs = json.dumps(prefs)
				open(pard['DATA_DIR'] + '/' + item + '/preferences.txt', 'w').write(json_prefs)
					
			except IOError:
				pass


def get_hex_color_list(pard):
	pard['color_list_search'] = pard.get('color_list_search', '')
	hex_color_list = [
		('#000000', 'Black'),
		('#000080', 'Navy'),
		('#00008B', 'DarkBlue'),
		('#0000CD', 'MediumBlue'),
		('#0000FF', 'Blue'),
		('#006400', 'DarkGreen'),
		('#008000', 'Green'),
		('#008080', 'Teal'),
		('#008B8B', 'DarkCyan'),
		('#00BFFF', 'DeepSkyBlue'),
		('#00CED1', 'DarkTurquoise'),
		('#00FA9A', 'MediumSpringGreen'),
		('#00FF00', 'Lime'),
		('#00FF7F', 'SpringGreen'),
		('#00FFFF', 'Cyan'),
		('#191970', 'MidnightBlue'),
		('#1E90FF', 'DodgerBlue'),
		('#20B2AA', 'LightSeaGreen'),
		('#228B22', 'ForestGreen'),
		('#2E8B57', 'SeaGreen'),
		('#2F4F4F', 'DarkSlateGray'),
		('#32CD32', 'LimeGreen'),
		('#3CB371', 'MediumSeaGreen'),
		('#40E0D0', 'Turquoise'),
		('#4169E1', 'RoyalBlue'),
		('#4682B4', 'SteelBlue'),
		('#483D8B', 'DarkSlateBlue'),
		('#48D1CC', 'MediumTurquoise'),
		('#4B0082', 'Indigo'),
		('#556B2F', 'DarkOliveGreen'),
		('#5F9EA0', 'CadetBlue'),
		('#6495ED', 'CornflowerBlue'),
		('#66CDAA', 'MediumAquaMarine'),
		('#696969', 'DimGray'),
		('#6A5ACD', 'SlateBlue'),
		('#6B8E23', 'OliveDrab'),
		('#708090', 'SlateGray'),
		('#778899', 'LightSlateGray'),
		('#7B68EE', 'MediumSlateBlue'),
		('#7CFC00', 'LawnGreen'),
		('#7FFF00', 'Chartreuse'),
		('#7FFFD4', 'Aquamarine'),
		('#800000', 'Maroon'),
		('#800080', 'Purple'),
		('#808000', 'Olive'),
		('#808080', 'Gray'),
		('#8470FF', 'LightSlateBlue'),
		('#87CEEB', 'SkyBlue'),
		('#87CEFA', 'LightSkyBlue'),
		('#8A2BE2', 'BlueViolet'),
		('#8B0000', 'DarkRed'),
		('#8B008B', 'DarkMagenta'),
		('#8B4513', 'SaddleBrown'),
		('#8FBC8F', 'DarkSeaGreen'),
		('#90EE90', 'LightGreen'),
		('#9370D8', 'MediumPurple'),
		('#9400D3', 'DarkViolet'),
		('#98FB98', 'PaleGreen'),
		('#9932CC', 'DarkOrchid'),
		('#9ACD32', 'YellowGreen'),
		('#A0522D', 'Sienna'),
		('#A52A2A', 'Brown'),
		('#A9A9A9', 'DarkGray'),
		('#ADD8E6', 'LightBlue'),
		('#ADFF2F', 'GreenYellow'),
		('#AFEEEE', 'PaleTurquoise'),
		('#B0C4DE', 'LightSteelBlue'),
		('#B0E0E6', 'PowderBlue'),
		('#B22222', 'FireBrick'),
		('#B8860B', 'DarkGoldenRod'),
		('#BA55D3', 'MediumOrchid'),
		('#BC8F8F', 'RosyBrown'),
		('#BDB76B', 'DarkKhaki'),
		('#C0C0C0', 'Silver'),
		('#C71585', 'MediumVioletRed'),
		('#CD5C5C', 'IndianRed'),
		('#CD853F', 'Peru'),
		('#D02090', 'VioletRed'),
		('#D19275', 'Feldspar'),
		('#D2691E', 'Chocolate'),
		('#D2B48C', 'Tan'),
		('#D3D3D3', 'LightGrey'),
		('#D87093', 'PaleVioletRed'),
		('#D8BFD8', 'Thistle'),
		('#DA70D6', 'Orchid'),
		('#DAA520', 'GoldenRod'),
		('#DC143C', 'Crimson'),
		('#DCDCDC', 'Gainsboro'),
		('#DDA0DD', 'Plum'),
		('#DEB887', 'BurlyWood'),
		('#E0FFFF', 'LightCyan'),
		('#E6E6FA', 'Lavender'),
		('#E9967A', 'DarkSalmon'),
		('#EE82EE', 'Violet'),
		('#EEE8AA', 'PaleGoldenRod'),
		('#F08080', 'LightCoral'),
		('#F0E68C', 'Khaki'),
		('#F0F8FF', 'AliceBlue'),
		('#F0FFF0', 'HoneyDew'),
		('#F0FFFF', 'Azure'),
		('#F4A460', 'SandyBrown'),
		('#F5DEB3', 'Wheat'),
		('#F5F5DC', 'Beige'),
		('#F5F5F5', 'WhiteSmoke'),
		('#F5FFFA', 'MintCream'),
		('#F8F8FF', 'GhostWhite'),
		('#FA8072', 'Salmon'),
		('#FAEBD7', 'AntiqueWhite'),
		('#FAF0E6', 'Linen'),
		('#FAFAD2', 'LightGoldenRodYellow'),
		('#FDF5E6', 'OldLace'),
		('#FF0000', 'Red'),
		('#FF00FF', 'Magenta'),
		('#FF1493', 'DeepPink'),
		('#FF4500', 'OrangeRed'),
		('#FF6347', 'Tomato'),
		('#FF69B4', 'HotPink'),
		('#FF7F50', 'Coral'),
		('#FF8C00', 'Darkorange'),
		('#FFA07A', 'LightSalmon'),
		('#FFA500', 'Orange'),
		('#FFB6C1', 'LightPink'),
		('#FFC0CB', 'Pink'),
		('#FFD700', 'Gold'),
		('#FFDAB9', 'PeachPuff'),
		('#FFDEAD', 'NavajoWhite'),
		('#FFE4B5', 'Moccasin'),
		('#FFE4C4', 'Bisque'),
		('#FFE4E1', 'MistyRose'),
		('#FFEBCD', 'BlanchedAlmond'),
		('#FFEFD5', 'PapayaWhip'),
		('#FFF0F5', 'LavenderBlush'),
		('#FFF5EE', 'SeaShell'),
		('#FFF8DC', 'Cornsilk'),
		('#FFFACD', 'LemonChiffon'),
		('#FFFAF0', 'FloralWhite'),
		('#FFFAFA', 'Snow'),
		('#FFFF00', 'Yellow'),
		('#FFFFE0', 'LightYellow'),
		('#FFFFF0', 'Ivory'),
		('#FFFFFF', 'White')
		]
	if pard['color_list_search']:
		result = []
		for item in hex_color_list:
			if item[1].lower().find(pard['color_list_search'].lower()) > -1:
				result.append(item)
	else:
		result = hex_color_list
	return result


def javascript(pard):
	js = """
<script type="text/javascript" language="javascript" charset="utf-8">

var isIE = (navigator.appName == "Microsoft Internet Explorer");
var isMac = (navigator.userAgent.indexOf("Mac") != -1);

function delete_row(row_id) {
	var r = $(row_id);
	r.remove();
	
	// delete row from table_array
	var pos = table_array[row_id];
	delete table_array[row_id];
	
	// renumber the table
	for (key in table_array) {
		if (table_array[key] > pos) {
			table_array[key] = table_array[key] - 1;
			$('ind_' + key).value = table_array[key];
			}
		}
	
	}
	
	
function move_down(row_id) {
	var the_row = $(row_id);
	var row_position = table_array[row_id];
	
	for (key in table_array) {
		pos = table_array[key];
		if (row_position + 1 == pos) {
		
			// Increments the index of current row
			table_array[row_id] = row_position + 1;
			$('ind_' + row_id).value = row_position + 1;
			
			// Decrements the index of next row
			table_array[key] = pos - 1;
			$('ind_' + key).value = pos - 1;
			
			// insert current row after next row
			next_row = $(key);
			next_row.insert({after: the_row});
			break;
			}
		}
	}


function move_up(row_id) {
	var the_row = $(row_id);
	var row_position = table_array[row_id];
	
	for (key in table_array) {
		pos = table_array[key];
		if (row_position - 1 == pos) {
		
			// Decrements the index of current row
			table_array[row_id] = row_position - 1;
			$('ind_' + row_id).value = row_position - 1;
			
			// Increments the index of previous row
			table_array[key] = pos + 1;
			$('ind_' + key).value = pos + 1;
			
			// insert current row before previous row
			prev_row = $(key);
			prev_row.insert({before: the_row});
			break;
			}
		}
	}

function select_color(row_index) {
	var area = 'area_' + row_index;
	params = "module=" + escape("%(module)s");
	params += "&sid=" + escape("%(sid)s");
	params += "&action=select_color";
	params += "&row_index=" + escape(row_index);
	_complete = 0;
	req = new Ajax.Request
	(
		"%(APPSERVER)s",
		{
			method: 'post',
			parameters: params,
			onLoading: function()
			{
				if (_complete == 0) {
					$(pard_js.area).innerHTML = '<img src="/img/progress.gif">';
				}
			},
			onComplete: function(resp)
			{
				_complete = 1;
				$(area).innerHTML = resp.responseText;
			}
		}
	)

	}


function redraw(field, evt)
{
	if (isIE && isMac) return false;
	
	var s = field.value;
	
	evt = (evt) ? evt : ((window.event) ? event : null);
	if (evt)
	{
		switch (evt.keyCode)
		{
			case 38: //up arrow  
			case 40: //down arrow
			case 37: //left arrow
			case 39: //right arrow
			case 33: //page up  
			case 34: //page down  
			case 36: //home  
			case 35: //end                  
			case 13: //enter  
			case 9: //tab  
			case 27: //esc  
			case 16: //shift  
			case 17: //ctrl  
			case 18: //alt  
			case 20: //caps lock
			//case 8: //backspace  
			//case 46: //delete
			   return false;
			   break;
		}
	}

	params = "module=" + escape("%(module)s");
	params += "&sid=" + escape("%(sid)s");
	params += "&action=select_color_redraw";
	params += "&color_list_search=" + escape($('color_list_search').value);
	req = new Ajax.Request
	(
		"%(APPSERVER)s",
		{
			method: 'post',
			parameters: params,
			//onLoading: function()
			//{
			//	$('color_list_table').innerHTML = '<img src="/img/progress.gif">';
			//},
			onComplete: function(resp)
			{
				$('color_list_table').innerHTML = resp.responseText;
			}
		}
	)

}


function set_color(hex_color) {
	var row_index = $('row_index').value;
	var color = 'color_' + row_index;
	var area  = 'area_' + row_index;
	var sample_color = 'sample_color_' + row_index;
	$(area).innerHTML = '';
	$(color).value = hex_color;
	$(sample_color).setStyle({ backgroundColor: hex_color});
	}

</script>
	""" % pard
	return js


def sort_by(l, n):
	nlist = map(lambda x, n=n: (x[n], x), l)
	nlist.sort()
	return map(lambda (key, x): x, nlist)


if __name__ == '__main__':
	import config
	pard = config.global_parameters({})
	convert_connection_history(pard)
	
	