import re
import sys
import os
import time
import string
import random
import shelve
import pprint
import calendar
import traceback
import copy
import datetime
from decimal import Decimal


def format_messaggio(msg=[]):
	"""Formatta una lista di messaggi in una tabella html"""
	if isinstance(msg, str):
		msg = [msg]
	if msg:
		tab_html = []
		tab_html.append('<table width="500" cellpadding="0" cellspacing="2" border="0">')
		for elem in msg:
			tab_html.append('<tr><td class=tb3blu>%s</td></tr>' % elem)
		tab_html.append('</table>')
		tab_html = string.join(tab_html, '\n')
		return tab_html # + '<br>'
	else:
		return ''


def get_time():
	now = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime(time.time()))
	return now
		

def dump(pard):
	import pprint
	print 'Content-type: text/html\n\n'
	print '<pre>'
	print '===================='
	#print pard['header']
	#print '===================='
	if type(pard) is type('string'):
		print pard
	else:
		pprint.pprint(pard)
	#s = ''
	#for k in pard.keys():
	#	s = s + '<b>' + k + '</b>: ' + repr(pard[k]) + '<br>\n'
	#print s
	print '===================='
	print '</pre>'


def make_pop(pop_name, lista, selected_value, jscript=''):
	html = '<select name="%s" id="%s" %s>' % (pop_name, pop_name, jscript)
	flag_selected = ''
	for item in lista:
		id = item[0]
		codice = item[1]
		selected = ''
		if item[0] == selected_value:
			selected = 'selected'
			flag_selected = 'FOUND'
		else:
			if flag_selected == '':
				try:
					if item[2] == 'def':
						selected = 'selected'
				except:
					pass
		html = html + '<option value="%s" %s>%s</option>' % (id, selected, codice)
	html = html + '</select>'
	return html


def dict_to_table(pard, dict_list, field_list=[], result_type=''):
	if field_list == []:
		field_list = dict_list[0].keys()
	l = []
	l.append('\t'.join(field_list))
	for d in dict_list:
		ll = []
		for key in field_list:
			if d[key] == None:
				d[key] = ''
			ll.append(str(d[key]))
		l.append('\t'.join(ll))
	
	if result_type == 'ftext':
		return table_to_text(pard, l)
	else:
		return '\n'.join(l)


def table_to_text(pard, ll):
	"""Data una lista di righe con i campi separati da tabulatori
	restitusce un testo formattato tipo query "mysql"
	"""
	column_lenght_dict = {}
	for line in ll:
		llc = line.split('\t')
		i = 0
		for col in llc:
			column_lenght_dict[i] = column_lenght_dict.get(i, 2)
			column_lenght_dict[i] = max(column_lenght_dict[i], len(col) + 2)
			i += 1
	sep = ['+']
	for i in range(len(ll[0].split('\t'))):
		sep.append('-'*column_lenght_dict[i])
		sep.append('+')
	sep = ''.join(sep)
	t = [sep]
	j = 0
	for line in ll:
		llc = line.split('\t')
		i = 0
		tl = ['|']
		for col in llc:
			tl.append(' %s |' % col.ljust(column_lenght_dict[i]-2))
			i += 1
		tl = ''.join(tl)
		t.append(tl)
		if j == 0 or j == (len(ll) - 1):
			t.append(sep)
		j += 1
	return '\n'.join(t)


def dict_to_insert(dict_list, field_list):
	l = ['insert into #replace_this_with_table_name#']
	l.append('(' + ', '.join(field_list) + ')')
	l.append('values')
	for d in dict_list:
		ll = []
		for key in field_list:
			s = d.get(key, '')
			if s == None:
				s = 'Null'
			elif isinstance(s, unicode):
				s = s.encode('utf8', 'replace')
				s = "'%s'" % s.replace("'", "''")
			elif isinstance(s, str):
				s = "'%s'" % s.replace("'", "''")
			elif isinstance(s, int) or  isinstance(s, float):
				s = str(s)
			elif isinstance(s, Decimal):
				s = str(s)
			else:
				s = str(s)
				s = "'%s'" % s.replace("'", "''")
				
			ll.append(s)
		l.append('(' + ', '.join(ll) + '),')
	res = '\n'.join(l)[:-1] # Tolgo l'ultima virgola
	return res


def build_exception(type=None, value=None, tb=None, limit=None):
	if type is None:
		type, value, tb = sys.exc_info()
	buf = "traceback (most recent call last):"
	list = traceback.format_tb(tb, limit) + traceback.format_exception_only(type, value)
	buf = buf + "%s\n%s" % ("".join(list[:-1]), list[-1])
	del tb
	return buf


def get_last_exception():
	type, value, tb = sys.exc_info()
	r = traceback.format_exception_only(type, value)[-1]
	if r[-1] == '\n':
		r = r[:-1]
	return r


def escape_javascript(s):
	s = s.replace('\\', '\\\\').replace("'", "\\'").replace('\n', '\\n').replace('\r', '\\r').replace('"', '\\"')
	return s


def sbianca(rec={}):
	for key in rec.keys():
		if rec[key] == '':
			rec[key] = '&nbsp;'
	return rec


def check_dir(dir_name):
    """Controlla se esiste la directory"""
    if os.path.exists(dir_name):
        if os.path.isdir(dir_name):
            pass
        else:
            os.remove(dir_name)
            os.mkdir(dir_name)
    else:
        os.mkdir(dir_name)


def sort_by(l, n, key=None):
    # case insensitive sort for string:
    # key = ci_sort = lambda (key, x): key.lower()
    nlist = map(lambda x, n=n: (x[n], x), l)
    nlist.sort(key=key)
    return map(lambda (key, x): x, nlist)
