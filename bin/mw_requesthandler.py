#! /usr/local/bin/python -u

from os import listdir
import signal
import socket
import pprint
import urllib

from twisted.internet import reactor, defer
from twisted.internet import threads
from twisted.web import resource, static, server, http

import driver

def get_header(request):
	d = {}
	header = request.getAllHeaders()
	for k in header:
		http_key = 'HTTP_%s' % k.upper().replace('-', '_')
		d[http_key] = header[k]
	d['REMOTE_ADDR'] = request.getClientIP()
	if d['REMOTE_ADDR'] == 'localhost':
		d['REMOTE_ADDR'] = '127.0.0.1'
	return d


class MyRequestHandler(resource.Resource):

	def __init__(self, query=''):
		resource.Resource.__init__(self)
		self.query = query
		self.isLeaf = True

	def render(self, request):
		self.dthread = threads.deferToThread(self.myHandler, request)
		self.dthread.addCallback(
			self.send_result, request).addErrback(
				self.send_error, request)
		return server.NOT_DONE_YET
		
	
	def myHandler(self, request):
		pard = {}
		pard['ERROR'] = ''
		pard['html'] = ''
		pard['file_upload'] = []
		pard['header'] = 'Content-type: text/html\n\n'
		request.setHeader('Content-Type', 'text/html')
		
		pard.update(get_header(request))
		
		if request.prepath[0] <> '':
			cookie_sid = request.getCookie('cookie_sid')
			pard['path_alias'] = request.prepath[0]
			#print 'cookie_sid', cookie_sid
			if cookie_sid:
				pard['sid'] = cookie_sid
				pard['module'] = 'qq'
				query = request.path[len(request.prepath[0])+2:]
				if query and query[-1] == '/':
					query = query[:-1]
				if query:
					pard['query'] = urllib.unquote_plus(query)
					pard['query_area'] = pard['query']
					pard['action'] = 'run_query'
					
		form = request.args
		for k in form.keys():
			if k not in pard: # disable environment key overwrite
				if (k[0:12] == 'file_upload_') and (form[k].filename != ''):
					pard['file_upload'].append({'filename': form[k].filename, 'content': form.getvalue(k, ''), 'form_id': k})
				else:
					pard[k] = form[k]
					if isinstance(pard[k], list) and len(pard[k]) == 1:
						pard[k] = pard[k][0]
		
		pard['HTTP_REFERER'] = pard.get('HTTP_REFERER', '')
		pard['REMOTE_ADDR'] = pard.get('REMOTE_ADDR', '')
		pard['HTTP_X_FORWARDED_FOR'] = pard.get('HTTP_X_FORWARDED_FOR', '')
		if pard['HTTP_X_FORWARDED_FOR']:
			pard['REMOTE_IP_ADDR'] = pard['HTTP_X_FORWARDED_FOR']
		else:
			pard['REMOTE_IP_ADDR'] = pard['REMOTE_ADDR']
		
		driver_result = driver.driver(pard)
		#pprint.pprint(driver_result)
		if driver_result['header'] == 'Content-type: text/html\n\n':
			pass
		else:
			for item in driver_result['header']:
				request.setHeader(item[0], item[1])
		result = driver_result['html']
		return result
		

	def send_result(self, result, request):
		request.write(result)
		request.finish()


	def send_error(self, failure, request):
		request.setResponseCode(http.INTERNAL_SERVER_ERROR)
		print failure.getTraceback()
		request.write('D\'OH!')
		#request.write("<pre>Error fetching posts: %s</pre>" % failure.getTraceback())
		request.finish()



class ServeDir(resource.Resource):

	def __init__(self, dir_path, valid_extension):
		resource.Resource.__init__(self)
		
		self.dir_list = []
		ll = listdir(dir_path)
		for fn in ll:
			if fn[-3:].lower() in valid_extension:
				self.dir_list.append(fn)
		
		for fn in self.dir_list:
			self.putChild(fn, static.File('%s/%s' % (dir_path, fn)))


# class QueryDir(resource.Resource):
# 
# 	def __init__(self):
# 		resource.Resource.__init__(self)
# 
# 	def getChild(self, path, request):
# 		return MyRequestHandler(path)
# 	
# 	def render(self, request):
# 		request.redirect(request.path + '/')
# 		return ''


class OpenSearchDir(resource.Resource):

	def __init__(self, appserver):
		resource.Resource.__init__(self)
		self.appserver = appserver

	def getChild(self, path, request):
		return OpenSearchHandler(self.appserver)
	
	def render(self, request):
		request.redirect(request.path + '/')
		return ''


class OpenSearchHandler(resource.Resource):

	def __init__(self, appserver):
		resource.Resource.__init__(self)
		self.appserver = appserver
	
	def render(self, request):
		try:
			opensearch_file = request.prepath[1]
			alias, ext = opensearch_file.split('.')
			if ext != 'xml':
				raise ValueError, 'Invalid Extension'
			xml = """<?xml version="1.0" encoding="UTF-8"?>
				<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
					<ShortName>MySQL Query Navigator: %s</ShortName>
					<Description>Query %s MySQL DataBase</Description>
					<Url type="text/html" method="GET" template="%s/%s/{searchTerms}" />
					<InputEncoding>UTF-8</InputEncoding>
				</OpenSearchDescription>
				""" % (alias, alias, self.appserver, alias)
			xml = xml.replace('\t', '')
			request.setHeader('Content-Type', 'text/xml')
			return xml
		except:
			request.setResponseCode(http.BAD_REQUEST)
			request.write("<pre>Bad Request</pre>")
			return ''


class MyResource(resource.Resource):
	
	def __init__(self, query=''):
		resource.Resource.__init__(self)

	def getChildWithDefault(self, path, request):
		# Tutti i path definiti vengono gestiti dagli handler
		# gli altri vengono gestiti da MyRequestHandler che avra'
		# settatto il flag isLeaf a true in modo da essere l'ultimo
		# valutato da getChildForRequest
		#
		# Una alternativa a questa soluzione era di sovrasrivere il
		# metodo getChildForRequest di Resource ...
		if self.children.has_key(path):
			return self.children[path]
		return MyRequestHandler(path)


if __name__ == '__main__':
	import config
	pard = config.global_parameters({})
	config.check_dir_tree(pard)
	#root = resource.Resource()
	root = MyResource() # Uso questa per poter sovrascivere il metodo getChildWithDefault
	root.putChild('', MyRequestHandler())
	#root.putChild('query', QueryDir())
	root.putChild('img', ServeDir(pard['IMG_DIR'], ('jpg', 'gif', 'png')))
	root.putChild('icons', ServeDir(pard['ICON_DIR'], ('png',)))
	root.putChild('javascripts', ServeDir('%(HTML_DIR)s/javascripts' % pard, ('.js',)))
	buf = open('%(HTML_DIR)s/sqlweb.css' % pard, 'r').read()
	root.putChild('sqlweb.css', static.Data(buf, 'text/css'))
	#root.putChild('OpenSearch.xml', static.File('%(HTML_DIR)s/OpenSearch.xml' % pard))
	root.putChild('OpenSearch', OpenSearchDir(pard['APPSERVER']))
	root.putChild('ColorList.css', static.File('%(HTML_DIR)s/ColorList.css' % pard))
	site = server.Site(root, '%(LOG_DIR)s/access.log' % pard)
	reactor.listenTCP(pard['HTTP_PORT'], site, interface=pard['IP_ADDRESS'])
	reactor.run()
	## ToDo 
	#    - aggiungere gestione processo

