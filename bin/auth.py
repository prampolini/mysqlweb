import os
import string
import shelve
import time
import random
import Cookie


#import db_access
import db
import qq
import tools
import preferences


def js_create_cookie(pard):
	pard['ALIAS'] = pard.get('ALIAS', '')
	if pard['ALIAS'] == '':
		return ''
	c = Cookie.SimpleCookie()
	c['cookie_sid'] = pard['sid']
	c['cookie_sid']['path'] = '/%(ALIAS)s' % pard
	tomorrow = time.time() + 86400
	expGMT = time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime(tomorrow))
	c['cookie_sid']['expires'] = expGMT
	return c.js_output()


def old_login(pard):
	pard['login_error'] = pard.get('login_error', '&nbsp;')
	pard['last_conn'] = load_last_connection(pard)
	user, host, db = pard['last_conn'].split('\t')
	pard['user'] = pard.get('user', user)
	pard['host'] = pard.get('host', host)
	pard['db'] = pard.get('db', db)
	if not os.path.isdir('%(DATA_DIR)s/%(REMOTE_IP_ADDR)s' % pard):
		os.mkdir('%(DATA_DIR)s/%(REMOTE_IP_ADDR)s' % pard, 0770)
	pard['conn_drop_down'], pard['javascript'] = make_connection_drop_down_list(pard)
	
	html = """
	<html>
	<head>
	%(CSS)s
	%(javascript)s
	</head>
	<body>
	<center>
	<form action="%(APPSERVER)s" method="post">
	<input type="hidden" name="module" value="auth">
	<input type="hidden" name="program" value="check_authentication">
	<input type="hidden" name="tmp_sid" value="%(tmp_sid)s">
	<table width="300">
	<tr>
		<td colspan="2" style="color: #ffffff; background: #003399;	text-align: center;">MySQL Login</td>
	</tr>
	
	<tr>
		<td colspan="2" align="center">%(conn_drop_down)s</td>
	</tr>
	
	<tr>
		<td><b>User:</b></td>
		<td><input type="text" name="user" value="%(user)s"></td>
	</tr>
	<tr>
		<td><b>Password:</b></td>
		<td><input type="password" name="password" value=""></td>
	</tr>
	<tr>
		<td><b>Host:</b></td>
		<td><input type="text" name="host" value="%(host)s"></td>
	</tr>
	<tr>
		<td><b>DB:</b></td>
		<td><input type="text" name="db" value="%(db)s"></td>
	</tr>
	<tr>
		<td colspan=2 align=center><input type="submit" name="login" value="Login"></td>
	</tr>
	<tr>
		<td colspan=2 align="center" valign="middle" bgcolor="#003399"><b>&nbsp;</b></td>
	</tr>
	<tr>
		<td colspan="2" class="err">%(login_error)s</td>
	</tr>
	</table>
	</form>
	</center>
	</body>
	</html>
	"""	
	pard['html'] = html % pard
	return pard


def login(pard):
	pard['login_error'] = pard.get('login_error', '&nbsp;')
	pard['last_conn'] = load_last_connection(pard)
	user, host, db = pard['last_conn'].split('\t')
	if 'path_alias' in pard and pard['path_alias']:
		pard['alias_pref'] = preferences.load_alias_pref(pard)
		if pard['alias_pref']:
			user = pard['alias_pref']['user']
			host = pard['alias_pref']['host']
			db = pard['alias_pref']['db']
			if 'STORE_PASSWORD' in pard and pard['STORE_PASSWORD']:
				passw = pard['alias_pref']['passw']
				pard['password'] = pard.get('password', passw)
	pard['password'] = pard.get('password', '')
	pard['user'] = pard.get('user', user)
	pard['host'] = pard.get('host', host)
	pard['db'] = pard.get('db', db)
	if not os.path.isdir('%(DATA_DIR)s/%(REMOTE_IP_ADDR)s' % pard):
		os.mkdir('%(DATA_DIR)s/%(REMOTE_IP_ADDR)s' % pard, 0770)
	pard['conn_drop_down'], pard['javascript'] = make_connection_drop_down_list(pard)
	
	pard['html'] = """
	<html>
	<head>
		%(CSS)s
		%(javascript)s
	</head>
	<body>
	<form action="%(APPSERVER)s" method="post">
	<input type="hidden" name="module" value="auth">
	<input type="hidden" name="program" value="check_authentication">
	<input type="hidden" name="tmp_sid" value="%(tmp_sid)s">
	<table width="100%%" class="header">
		<tr>
			<td class="left">Welcome to MySQL <em>Query Navigator</em></td>
		</tr>
	</table>
	
	<table width="100%%">
	<tr>
		<td width="70%%" style="vertical-align: top; line-height: 170%%;">
			<h2>MySQLweb is a web application that run queries, create and edit data, and view and export results.</h2>
			
			<img src="/icons/database_gear.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>DataBase Structure</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Database Structure Browser, provides instant access to database schema and objects.
			<br /><br />
			
			<img src="/icons/text_list_bullets.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Connection Manager</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;The Database Connections Panel enables developers to create, organize, and manage database connections
			<br /><br />
			
			<img src="/icons/clock.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>History Panel</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;The History Panel provides complete session history of queries showing what queries were run and when. Developers can easily retrieve, review, re-run, append or modify previously executed SQL statement.
			<br /><br />

			<img src="/icons/lightning.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Run current query</b>&nbsp;&nbsp;<br>
			<br />

			<img src="/icons/lightning_go.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Run selected query</b>&nbsp;&nbsp;<br>
			<br />

			<img src="/icons/page_white_code.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Comment selection</b>&nbsp;&nbsp;<br>
			<br />

			<img src="/icons/script_lightning.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Exec Procedures</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Multiple queries can be executed simultaneously (in transaction - query have to be separated by semicolon)
			<br /><br />

			<img src="/img/save.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Save SQL</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Developers can save and easily reuse <img src="/icons/folder_page.png" border="0"> common Queries
			<br /><br />

			<img src="/icons/page_white_put.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Dump SQL</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Dumps results of a query to a file contains SQL insert statements.
                        These stataments can be used for backup or transfer data to another SQL server.
			<br /><br />

			<img src="/icons/star.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Query Autocomplete</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Press "esc" when you are typing queries and enjoy with <em>Autocoplete-query</em> feature!
			<br /><br />
			
			<img src="/icons/star.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Query Wizard</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Type <code>wsel tablename [,tablename 2, ...]</code> and <em>run query</em>: MySQLweb prepares for you select statement to query the specified tables.<br />
			&nbsp;&nbsp;Type <code>wins tablename</code> and <em>run query</em>: MySQLweb prepares for you insert statement.
			<br /><br />
			
			<img src="/icons/star.png" border="0" style="float: left">
			&nbsp;&nbsp;<b>Silk Icons Search</b>&nbsp;&nbsp;<br>
			&nbsp;&nbsp;Type <code>silk icon-pattern</code> and <em>run query</em>: MySQLweb search <em>Silk</em> icon that matches the pattern specified.
			<br /><br />
									
		</td>
		<td width="30%%" style="vertical-align: top;">
			
			
	<table width="300">
	<tr>
		<td colspan="2" style="font-weight: bold; color: #2a68c8; text-align: center;">MySQL Login</td>
	</tr>
	
	<tr>
		<td colspan="2" align="center">
			%(conn_drop_down)s&nbsp;&nbsp;
			<a href="%(APPSERVER)s?module=preferences" title="Connection Preferences">
			<img src="/icons/application_view_list.png" border="0"></a>
		</td>
	</tr>
	
	<tr>
		<td><b>User:</b></td>
		<td><input type="text" name="user" value="%(user)s"></td>
	</tr>
	<tr>
		<td><b>Password:</b></td>
		<td><input type="password" name="password" value="%(password)s"></td>
	</tr>
	<tr>
		<td><b>Host:</b></td>
		<td><input type="text" name="host" value="%(host)s"></td>
	</tr>
	<tr>
		<td><b>DB:</b></td>
		<td><input type="text" name="db" value="%(db)s"></td>
	</tr>
	<tr>
		<td colspan=2 align=center><input type="submit" name="login" value="Login" class="btn"></td>
	</tr>
	<tr>
		<td colspan="2" class="err">%(login_error)s</td>
	</tr>
	</table>
			
			
			
			
		</td>
	</tr>
	</table>

	<table width="100%%" class="header">
		<tr>
			<td class="center">GF&copy;</td>
		</tr>
	</table>
	</form>
	</body>
	</html>
	""" % pard
	
	return pard
	

def check_authentication(pard):
	pard['HOST'] = pard['host']
	pard['USER'] = pard['user']
	pard['PASSWORD'] = pard['password']
	pard['DB'] = pard['db']
	try:
		conn = db.db_access.connect(pard)
		conn.close()
		flag = 'OK'
		pard = load_user_data(pard)
		sid_file = sid_get(pard)
		sid_file['sid_user'] = pard['USER']
		sid_file['sid_password'] = pard['PASSWORD']
		sid_file['sid_host'] = pard['HOST']
		sid_file['sid_db'] = pard['DB']
		sid_file['sid_alias'] = pard['ALIAS']
		sid_file['sid_color'] = pard['COLOR']
		sid_file['sid_time'] = time.time()
		sid_file['sid_buffer'] = {}
		try:
			result = sid_set(pard, sid_file)
			pard = load_sid_data(pard)
			flag = 'OK'
		except:
			pard['tmp_sid'] = calc_sid(pard)
			flag = 'STOP'
		set_last_conn(pard)
		set_connection_history(pard)

	except:
		pard['login_error'] = tools.get_last_exception()
		flag = 'STOP'	
	
	if flag == 'OK':
		pard = qq.main(pard)
	else:
		pard = login(pard)
	return pard



def load_user_data(pard):
	prefs = preferences.load_preferences(pard)
	for rec in prefs:
		if  rec['host'] == pard['HOST'] \
		and rec['user'] == pard['USER'] \
		and rec['db'] == pard['DB']:
			pard['ALIAS'] = rec['alias']
			pard['COLOR'] = rec['color']
			break
	else:
		pard['ALIAS'] = ''
		pard['COLOR'] = pard['DEFAULT_COLOR']
	return pard


def calc_sid(pard):
	sid = string.replace(repr(time.time()), '.', '') + str(random.randint(0, 9999))
	return sid


def load_sid_data(pard):
	sid_file = sid_get(pard)
	try:
		pard['USER'] = sid_file['sid_user']
		pard['PASSWORD'] = sid_file['sid_password']
		pard['HOST'] = sid_file['sid_host']
		pard['DB'] = sid_file['sid_db']
		pard['ALIAS'] = sid_file['sid_alias']
		pard['COLOR'] = sid_file['sid_color']
		pard['sid_buffer'] = sid_file['sid_buffer']
	except:
		pass
	return pard


def check_sid(pard):
	try:
		if pard['HTTP_X_FORWARDED_FOR']:
			ip = pard['HTTP_X_FORWARDED_FOR'].split(',')[0].strip()
		else:
			ip = pard['REMOTE_ADDR']
		if ip not in pard['AUTH_IP']:
			pard['flag_sid'] = 'KO'
			return pard
	except:
		pard['flag_sid'] = 'KO'
		return pard
	
	timeout = 1
	start = time.time()
	while 1:
		stop = time.time()
		if stop - start > timeout:
			break
		try:
			if pard.has_key('tmp_sid'):
				pard['sid'] = pard['tmp_sid']
				flag_sid = 'OK'
			elif pard.has_key('sid'):
				sid_file = sid_get(pard)
				if (time.time() - sid_file['sid_time']) < pard['SID_TIMEOUT']:
					sid_file['sid_time'] = time.time()
					flag_sid = 'OK'
				else:
					flag_sid = 'NEW'
				try:
					sid_file['menu_selected'] = pard['menu_id']
				except:
					pass
				result = sid_set(pard, sid_file)
			else:
				flag_sid = 'NEW'
			pard['flag_sid'] = flag_sid
			break
		except:
			pard['flag_sid'] = 'NEW'
	return pard


def sid_get(pard):
	d = {}
	sid_file = shelve.open(pard['SID_DIR'] + '/' + pard['sid'] + '.sid')
	for key in sid_file.keys():
		d[key] = sid_file[key]
	sid_file.close()
	return d
	

def sid_set(pard, dict):
	#dict = string.replace(repr(dict), "'", "''")
	sid_file = shelve.open(pard['SID_DIR'] + '/' + pard['sid'] + '.sid')
	for key in dict.keys():
		sid_file[key] = dict[key]
	sid_file.close()


# def load_connection_history(pard):
# 	try:
# 		fp = open('%(DATA_DIR)s/%(REMOTE_IP_ADDR)s/conn_history.txt' % pard, 'r')
# 		buf = fp.read()
# 		fp.close()
# 		connection_history = buf.split('\n')[:-1]
# 	except IOError:
# 		connection_history = []
# 	return connection_history	

# def set_connection_history(pard):
# 	connection_history = load_connection_history(pard)
# 	current_connection = '%(USER)s\t%(HOST)s\t%(DB)s' % pard
# 	if current_connection not in connection_history:
# 		fp = open('%(DATA_DIR)s/%(REMOTE_IP_ADDR)s/conn_history.txt' % pard, 'a')
# 		fp.write(current_connection + '\n')
# 		fp.close()


def load_connection_history(pard):
	prefs = preferences.load_preferences(pard)
	connection_history = []
	for rec in prefs:
		connection_history.append('%(user)s\t%(host)s\t%(db)s\t%(passw)s' % rec)
	return connection_history	


def set_connection_history(pard):
	connection_history = load_connection_history(pard)
	current_connection = '%(USER)s\t%(HOST)s\t%(DB)s' % pard
	found = False
	for connection in connection_history:
		if current_connection in connection:
			found = True
	if not found:
		rec = {}
		rec['host'] = pard['HOST']
		rec['user'] = pard['USER']
		rec['passw'] = pard['PASSWORD']
		rec['db'] = pard['DB']
		preferences.add_row(pard, rec)


def load_last_connection(pard):
	try:
		fp = open('%(DATA_DIR)s/%(REMOTE_IP_ADDR)s/last_conn.txt' % pard, 'r')
		last_conn = fp.read()
		fp.close()
	except IOError:
		last_conn = '\t\t'
	return last_conn	


def set_last_conn(pard):
	fp = open('%(DATA_DIR)s/%(REMOTE_IP_ADDR)s/last_conn.txt' % pard, 'w')
	current_connection = '%(USER)s\t%(HOST)s\t%(DB)s' % pard
	fp.write(current_connection)
	fp.close()


def make_connection_drop_down_list(pard):
	connection_history = load_connection_history(pard)
	drop_down_list = [('0', 'Select Connection')]
	pard['ConnList'] = []
	ind = 0
	for elem in connection_history:
		ind += 1
		user, host, db, passw = elem.split('\t')
		pard['ConnList'].append("['%s', '%s', '%s', '%s']" % (user, host, db, passw))
		drop_down_list.append((str(ind), '%s@%s/%s' % (user, host, db)))
	pard['ConnList'] = "[['', '', ''], " + ', '.join(pard['ConnList']) + ']'
	js = """
	<script type="text/javascript" language="javascript" charset="utf-8">
	function setmulti(i)
		{
			ConnList = %(ConnList)s;
			document.forms[0].elements['user'].value = ConnList[i][0];
			document.forms[0].elements['host'].value = ConnList[i][1];
			document.forms[0].elements['db'].value = ConnList[i][2];
			document.forms[0].elements['password'].value = ConnList[i][3];
		}
	</script>
	""" % pard
	drop_down = tools.make_pop('conn_drop_down', drop_down_list, '', "onChange='setmulti(this.value)'")
	return drop_down, js


def error_manager(pard):
	if pard['ERROR'] == 'MISSING_SID':
		pard['error_message'] = '<b>Page not found</b>'
	else:
		pard['error_message'] = "<b>Error.</b>"
	errtext = string.split(pard['ERROR'], '\n')[-2]
	errtext = string.replace(errtext, '<B>', '')
	errtext = string.replace(errtext, '</B>', '')
	pard['errtext'] = errtext
	pard['html'] = """
		<html>
		<head>
		%(CSS)s
		<title>%(TITLE)s - ERROR</title>
		</head>
		<center>
		<body>	
		<!--
		%(ERROR)s
		-->
		%(error_message)s
		<br>
		<br>
		<b>%(errtext)s</b>
		</center>
		</body>
		</html>
	""" % pard
	return pard


def build_not_authorized(pard):
	if pard['flag_sid'] == 'KO':
		pard['msg'] = 'Not authorized!'
	else:
		pard['msg'] = 'Page not found.'
	html = """
	<html>
	<head>
	<title>%(TITLE)s</title>
	</head>
	<body>
	<center>
	<h3>%(msg)s</h3>
	</center>
	</body>
	</html>
	"""
	pard['html'] = html % pard
	return pard



if __name__ == '__main__':
	import config
	pard = config.global_parameters(pard={})
# 	pard['module'] = 'admin'
# 	pard['program'] = 'utontiii'
# 	pard['menu_operation_tab'] = get_operation_recursive(pard)
# 	tools.dump(pard['menu_operation_tab'])
# 	t = get_menu_bin(pard)
# 	tools.dump(t)
	pard['id_utente'] = 'giames'
	load_user_data(pard)
	